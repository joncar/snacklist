<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <div class="container-fluid fondo-gradient-azul contenedor-preguntas">
        <div class="container fondo-preguntas">
            <div class="row">
                <div class="col-md-12 text-center"><h2><b>Contáctanos</b></h2></div>
            </div>

            <div class="row text-center contenedor-datos-editar">
                <div class="col-sm-6 col-sm-offset-3">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Nombre" maxlength="8" type="text">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Teléfono" maxlength="8" type="tel">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Mensaje" rows="40" cols="50" type="text">
                </div>

                <div class="col-sm-4 col-sm-offstet-4 text-center btn-menu-top btn-general btn-derecha pull-right center-block">
                    <b>Enviar mis comentarios</b>
                </div>
            </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
</body>
</html>
