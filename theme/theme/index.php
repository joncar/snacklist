<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <!-- Slider TOP -->
    <div class="container-fluid contenedor-slider-top fadeOut owl-carousel owl-theme">
        <?php include('Carrousel/slider-top.php');?>
    </div>

    <!-- Snacktrend -->
    <div class="container-fluid fondo-gradient-azul">
        <?php include('snacktrend.php');?>
    </div>

    <!-- Slider Categorias -->
    <div class="container-fluid contenedor-slider-categorias" id="myHeader">
          <?php include('Carrousel/slider-categorias.php');?>
    </div>

    <!-- Slider Entretenimiento -->
    <div class="container-fluid fondo-gradient-gris contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-entretenimiento.php');?>
    </div>

    <!-- Slider Gris Gastronomia -->
    <div class="container-fluid fondo-gradient-gris-oscuro contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-gastronomia.php');?>
    </div>

    <!-- Slider Moda -->
    <div class="container-fluid fondo-gradient-gris contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-moda.php');?>
    </div>

    <!-- Slider Gris Interesante -->
    <div class="container-fluid fondo-gradient-gris-oscuro contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-interesante.php');?>
    </div>

    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <!-- Slider Musica -->
    <div class="container-fluid fondo-gradient-gris contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-musica.php');?>
    </div>

    <!-- Slider Gris Tecnología -->
    <div class="container-fluid fondo-gradient-gris-oscuro contenedor-listado-tarjetas">
        <?php include('Carrousel/slider-tecnologia.php');?>
    </div>

    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

</body>
</html>
