<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon.ico">
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="shortcut icon" href="img/favicon.ico">

    <title>Snacklist V2</title>
    <!--  Fonts and icons  -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <link rel="stylesheet" href="assets/css/material-kit.css?v=2.0.3">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/queries.css">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <!-- Slider -->
    <link rel="stylesheet" href="Slider/owl.carousel.css">
    <link rel="stylesheet" href="Slider/owl.theme.default.min.css">
    <script src="Slider/jquery.min.js"></script>
    <script src="Slider/owl.carousel.js"></script>
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="Slider/owl.carousel.min.css">
    <link rel="stylesheet" href="Slider/owl.theme.default.min.css">
    <!-- Drag & Drop -->
    <link href='Drag-drop/example.css' rel='stylesheet' type='text/css' />
</head>
