<?php
	$categoria = $this->db->get_where('categorias',array('id'=>$c->categorias_id));
	if($categoria->num_rows()):
	$categoria = $categoria->row();
?>
<div class="fondo-gradient-gris">
	<div class="container-fluid">
		<div class="row margin-titulo-slider-home">
			<div class="col-xs-12">
				<h2 class="titulo-gastronomia texto-blanco">
				<a href="<?= base_url('categoria/'.toUrl($categoria->id.'-'.$categoria->nombre)) ?>">
					<img src="<?= base_url('img/categorias/'.$categoria->icono) ?>">
					<?= $categoria->nombre ?>
				</a>
				</h2>
			</div>
		</div>
	</div>
	<div id="slider-gris-01">
		<div class="container-fluid">
		<div class="owl-carousel owl-theme slider_categorias_home">
		<!-- Tarjeta -->
		
		<?php			
			foreach($this->querys->get_listas(array('listas_categorias.categorias_id'=>$categoria->id))->result() as $ca):
		?>			
			<div class="item  tarjetero">
				<?php $this->load->view('views/_lista_item',array('lista'=>$ca)); ?>

			</div>
		
		<?php endforeach ?>
		</div>		
		</div>
	</div>
	<?php endif ?>
</div>
<script>
	$('.slider_categorias_home').owlCarousel({
		//stagePadding: 50,
		loop:false,
		margin:10,
		nav:false,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:4
			},
			1200:{
				items:5
			}
		}
	});
</script>
