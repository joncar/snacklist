<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class ShortCodes extends Main{
        function __construct() {
            parent::__construct();
        }

        public function buscarImagenGoogle2(){
        	if(!empty($_GET['q'])){		
				echo '<h1>Resultado</h1><hr>';
				$img = file_get_contents('http://www.google.es/search?q='.urlencode($_GET['q']).'&source=lnms&tbm=isch');					
				preg_match_all('/<img[^>]+>/i',$img, $result); 
				echo '<div class="row">';				
				foreach($result[0] as $r){
					preg_match_all('/src="([^"]*)"/i', $r,$link);
					echo '<div class="col-xs-12 col-sm-6"><a href="javascript:seleccionarImagenGoogle(\''.$link[1][0].'\')">'.$r.'</a></div>';
				}
				echo '</div>';		
				//print_r($img);
			}
        }

         public function buscarImagenGoogle(){
        	if(!empty($_GET['q'])){		
				echo '<h1>Resultado</h1><hr>';

				$url='https://www.google.es/search?hl=es&tbm=isch&source=hp&biw=1474&bih=790&ei=ALQOXKT_HIGorgS5pJKIAg&q='.urlencode($_GET['q']);
				$agent= 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0';

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_USERAGENT, $agent);
				curl_setopt($ch, CURLOPT_URL,$url);
				$result=curl_exec($ch);
				//var_dump($result);
				preg_match_all('/<div class="rg_meta notranslate">(.*)<\/div>/', $result, $img); 		
				$img = fragmentar($img[0][0],'{','}',false);		
				$images = array();
				foreach($img as $i){
					$images[] = json_decode($i);
				}
				//print_r($images);

				echo '<div class="row">';				
				foreach($images as $r){			
					if(!empty($r->ou)){		
						echo '
						<div class="col-xs-12 col-sm-6">
							<a href="javascript:seleccionarImagenGoogle(\''.$r->ou.'\',\''.$r->ou.'\')">
								<img src="'.$r->ou.'" style="width:100%;">
							</a>
						</div>';
					}
				}
				echo '</div>';
			}
        }

        
        
    }
?>
