<?php 
  foreach ($list as $num_row =>$row): 
    $cat = get_instance()->db->get_where('categorias',['id'=>$row->id])->row();

?>
   <div class="col-xs-12 col-sm-6 col-md-3">

      
      <a href="<?= base_url('categoria/'.toUrl($cat->id.'-'.$cat->nombre)) ?>">
        <div class="header-categoria" style="display: flex;">
          <div><img src="<?= base_url('img/categorias/'.$cat->icono) ?>" alt="Categoria Snacktrend" class="img-responsive img-circle"></div>
          <div><b><?= $row->nombre ?> (<?= $row->total ?>)</b></div>
        </div>
      </a>
      


      <div class="tabla-categorias">
          <?php foreach($row->subcategorias->result() as $s): ?>
              <div class="contenedor-tabla-categoria">
                <b>
                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>">
                  <?php 
                    get_instance()->db->group_by('listas_id, listas_categorias.subcategorias_id'); 
                    echo get_instance()->db->get_where('listas_categorias',array('subcategorias_id'=>$s->id,'original'=>0))->num_rows(); 
                  ?>
                    
                  </a>
              </b>

                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?= $s->nombre ?></a>
              </div>
          <?php endforeach ?>
      </div>

  </div>
<?php endforeach ?>
<?php if(count($list)==0): ?>

   <div class="col-xs-12 col-sm-12 titulo-categorias text-center">
        <big><b>En estos momentos no existen subcategorias para esta categoria</b></big>
   </div>

<?php endif ?>
