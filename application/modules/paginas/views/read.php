<?php       
	$response = empty($_SESSION['msj'])?'':$_SESSION['msj'];
	$page = str_replace('[response]',$response,$page); 
	unset($_SESSION['msj']);

	$response = empty($_SESSION['msj2'])?'':$_SESSION['msj2'];
	$page = str_replace('[msg2]',$response,$page); 
	unset($_SESSION['msj']); 

	//Reemplace			
	$page = str_replace('[menu]',$this->load->view($this->theme.'menu',array(),TRUE),$page);		
	$page = str_replace('[footer]',$this->load->view($this->theme.'footer',array(),TRUE),$page);		
	$page = str_replace('[base_url]',base_url(),$page);		
	

	$this->db->limit(3);
	$this->db->order_by('fecha','DESC');
	$this->db->where('idioma',$_SESSION['lang']);
	$blog = $this->db->get_where('blog',array('status'=>1));
	foreach($blog->result() as $nn=>$b){
		$blog->row($nn)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
        $blog->row($nn)->foto = base_url('img/blog/'.$b->foto);
        $blog->row($nn)->fecha = strftime('%b %d, %Y',strtotime($b->fecha));
        $blog->row($nn)->texto = strip_tags($b->texto);
        $blog->row($nn)->texto = cortar_palabras($b->texto,20).'...';
        $blog->row($nn)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();            
        $blog->row($nn)->descripcion = cortar_palabras(strip_tags($b->texto),30);
        $blog->row($nn)->titulo = cortar_palabras(strip_tags($b->titulo),20);
        $blog->row($nn)->posicion = $nn==0?'left':'';
        $blog->row($nn)->posicion = $nn==1?'center':$b->posicion;
        $blog->row($nn)->posicion = $nn==2?'right':$b->posicion;
	}

	$page = $this->querys->fillFields($page,array('blog'=>$blog));		
    echo $page;
?>