<!-- Item 01 -->
<div class="item background-cover fondo-slider01 margin-secciones-menu-top" <?= !empty($categoria->foto)?'style="background:url(\''.base_url('img/categorias/'.$categoria->foto).'\'); background-repeat:no-repeat; background-size:cover;"':''; ?>>
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1><?= $categoria->nombre ?></h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <?php
          $this->db->select('sub_categorias.id, sub_categorias.nombre, COUNT(listas_categorias.id) AS total');
          $this->db->join('sub_categorias','sub_categorias.id = listas_categorias.subcategorias_id');
          $this->db->group_by('listas_categorias.subcategorias_id');
          $this->db->limit(3);
          $this->db->order_by('COUNT(listas_categorias.id) DESC');
          foreach($this->db->get_where('listas_categorias',array('sub_categorias.categorias_id'=>$categoria->id,'original'=>0))->result() as $s):
            $this->db->group_by('listas_id');
            $total = $this->db->get_where('listas_categorias',array('subcategorias_id'=>$s->id,'original'=>0))->num_rows();
        ?>
          <div class="col-md-4">
              <div class="listado-slider-interna">
                <a href="<?= base_url('subcategoria/'.toUrl($s->id.'-'.$s->nombre)) ?>"><?= $s->nombre ?><br>(<?= $total ?>)</a>
              </div>
          </div>
        <?php endforeach ?>

    </div>
</div>

<!-- Snacktrend -->
<div class="container-fluid fondo-gradient-rosa">
    <?php $this->db->where('categorias.id',$categoria->id); ?>
    <?php include('snacktrend.php');?>
</div>

<?php 
$this->db->order_by('sub_categorias.nombre','ASC'); 
foreach($this->db->get_where('sub_categorias',array('categorias_id'=>$categoria->id))->result() as $n=>$c): 
  $listas = $this->db->get_where('listas_categorias',array('subcategorias_id'=>$c->id,'original'=>0));
  
?>

    <div class="container-fluid contenedor-ads fondo-gradient-gris-<?= $n%2==0?'oscuro':'claro' ?>">
        <div>


   
            <div class="row titulo-redeem texto-blanco">
              <div class="col-sm-12">
                  <h3><b>
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="#">Categoría: <?= $categoria->nombre ?></a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Subcategoría: <?= $c->nombre ?> (<?= $listas->num_rows(); ?>)</li>
                      </ol>
                    </nav>
                  </b></h3>
              </div>
            </div>

            
            <div class="row">

              <?php
                $listas = $this->querys->get_listas(array('sub_categorias.id'=>$c->id,'original'=>0));
                foreach($listas->result() as $l):
              ?>
                <div class="col-sm-3 margin-bottom-listas-nuevas">
                  <?php get_instance()->load->view('views/_lista_item',array('lista'=>$l)); ?>
                </div>
              <?php endforeach ?>


            </div>

        </div>
    </div>
  
<?php endforeach ?>



    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
