<div class="container-fluid fondo-gradient-azul contenedor-preguntas">
    <div class="container fondo-preguntas">
        <?= $output ?>
    </div>
</div>


<!-- Footer -->
<footer class="container-fluid footer contenedor-footer">
  <?php include('footer.php');?>
</footer>

<!-- Mapa de sitio -->
<footer class="container-fluid contenedor-mapa-sito-footer">
    <?php include('footer-mapa.php');?>
</footer>

<!-- Modales -->
<?php include('modales.php');?>

<!-- Librerias -->
<?php include('librerias.php');?>
