<div class="row">
    <h2 class="titulo-gastronomia texto-blanco"><img src="assets/img/Iconos/icono-gastronomia.png">Moda y Belleza</h2>
</div>

<div id="slider-gris-01">
    <div class="row owl-carousel owl-theme slider-gris-01">
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="item item-slider-tarjetas">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>

    </div>
</div>

<script>
$('.slider-gris-01').owlCarousel({
    stagePadding: 50,
    loop:true,
    margin:10,
    nav:false,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>
