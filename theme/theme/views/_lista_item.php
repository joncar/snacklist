<div class="contenedor-tarjeta-blanco fondo-blanco">
          <div class="padding0" style="display: flex">
            <div class="" title="Vistas de la Lista" style="width:100%">
              <div class="vistas-tarjetas icon-vistas">
                <i class="fa fa-eye" aria-hidden="true"></i> <?= $lista->vistas ?>
              </div>
            </div>
            <div title="Fecha de creación de la Lista" style="text-align: right; width:100%">
              <div class="fecha-tarjetas icon-vistas">
                <?= date("d/m/Y",strtotime($lista->fecha)) ?>                
              </div>
            </div>
          </div>

          <div class="padding0" style="padding-right:0px; padding-left:0px;">

            <span class="categoria-snacktrend">
              <a href="<?= base_url('categoria/'.toUrl($lista->catid.'-'.$lista->catnom)) ?>"><?= $lista->catnom; ?></a>
              /
              <a href="<?= base_url('subcategoria/'.toUrl($lista->subcatid.'-'.$lista->subcatnom)) ?>"><?= $lista->subcatnom; ?></a>
            </span>

            <a href="<?= base_url('lista/'.toUrl($lista->id.'-'.$lista->titulo_corto)) ?>" class="link-tarjetas">
              <div class="header-tarjeta-categoria"><h3><?= $lista->titulo ?></h3></div>

              <div class="height-listas-principales">
                <?php foreach($this->querys->get_sort($lista->id) as $detall):
                  $detall->foto = $this->querys->get_foto($detall->adjunto);
                ?>
                <div>
                  <div class="imagen-listado-tarjeta" style="background:url('<?= $this->querys->get_foto($detall->adjunto) ?>'); background-size: cover; background-position:center center; width: 60px;height: 60px;">
                    <div class="top-listado-tarjeta texto-blanco" style="width: 60px;height: 60px;"><?= $detall->posicion ?></div>
                  </div>
                  <div class="listado-tarjeta"><b><?= $detall->nombre ?></b><br><?= $detall->autor ?></div>
                </div>
                <?php endforeach ?>
              </div>

            </a>

          </div>
        </div>
        <div class="titulo-tarjeta-categoria texto-gris-oscuro">
          <div class="perfil-tarjeta-categorias">
            <img src="<?= $lista->perfil ?>" alt="Perfil Snacktrend" class="img-responsive img-circle" style="width:30px; height:30px;">
          </div>
          <div class="username-tarjetas"><b> <?= $lista->username ?> </b><br>Votos:<?= $lista->votos ?></div>
        </div>