<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <div class="container-fluid contenedor-snackteam fondo-blanco">
        <div class="container">
            <div class="row titulo-redeem texto-negro"><h2>Editar lista</h2></div>

            <div class="row">
                <div class="col-sm-12">
                    Título de la lista
                    <h3><b>Mejores canciones rockeras de todos los tiempos de 1990 a 2010 en México</b></h3>
                </div>
                <div class="col-sm-12">
                    Título corto
                    <h3><b>Mejores canciones rockeras</b></h3>
                </div>
            </div>

            <div class="row titulo-redeem texto-negro"><h2>Clasificación</h2></div>

            <div class="row contenedor-select-publicar">
                <div class="col-sm-12 padding0" id="participantTable">
                    <div class="participantRow">
                        <div class="col-sm-6">
                            <h3>Categoría</h3>
                            <div class="wrap-select">
                      					<div id="dd" class="wrapper-dropdown-3">
                        						<span>Selecciona</span>
                        						<ul class="dropdown">
                        							<li><a href="#">01</a></li>
                        							<li><a href="#">02</a></li>
                        							<li><a href="#">03</a></li>
                        							<li><a href="#">04</a></li>
                        						</ul>
                      					</div>
                    				</div>
                        </div>

                        <div class="col-sm-6">
                            <h3>Subcategoría</h3>
                            <div class="wrap-select">
                      					<div id="dd2" class="wrapper-dropdown-3">
                        						<span>Selecciona</span>
                        						<ul class="dropdown">
                        							<li><a href="#">01</a></li>
                        							<li><a href="#">02</a></li>
                        							<li><a href="#">03</a></li>
                        							<li><a href="#">04</a></li>
                        						</ul>
                      					</div>
                    				</div>
                        </div>
                        <button class="remove btn-remover-categoria" type="button"><b>Eliminar</b></button>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div id="addButtonRow"><button class="add btn-agregar-categoria" type="button"><b>Agregar</b></button></div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 texto-input-publicar">Arrastra cualquier elemento para reposicionarlo</div>
                <div class="col-sm-12 contenedor-tabs-redeem">
                    <div class='parent'>
                      <div class='wrapper'>
                        <div id='left-rm-spill' class='container-drag'>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    1<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    2<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    3<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    4<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    5<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    6<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    7<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    8<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    9<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                            <!-- Tarjeta publicar -->
                            <div class="col-sm-12 contenedor-tarjeta-redeem">
                                <div class="col-sm-1 numero-votar">
                                    <i class="material-icons">arrow_drop_up</i><br>
                                    10<br>
                                    <i class="material-icons">arrow_drop_down</i>
                                </div>
                                <div class="col-sm-8">
                                    <div class="col-sm-12"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Nombre del elemento" type="text"></div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                  <i class="material-icons">camera_alt</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                  <i class="material-icons">play_arrow</i>
                                            </div>

                                            <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                  <i class="material-icons">gif</i>
                                            </div>
                                        </div>
                                        <div class="col-sm-8" style="padding-left:0px; padding-right:0px;"><input class="form-control margin-login-input" id="input-publicar-lista" placeholder="Autor de la imagen" type="text"></div>
                                    </div>
                                </div>
                                <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                    <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                </div>
                            </div>

                        </div><!-- Termina div Drag & Drop -->
                      </div>
                    </div>
                </div>
                <div class="col-sm-12 contenedor-tabs-redeem">
                  <a href="reordenar.php"><div class="col-sm-3 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Guardar cambios</b></div></a>
                </div>
            </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Select Input -->
    <script>
    function DropDown(el) {
			this.dd = el;
			this.placeholder = this.dd.children('span');
			this.opts = this.dd.find('.dropdown a');
			this.val = '';
			this.index = -1;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;
				obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					return false;
				});
				obj.opts.on('click',function(){
					var opt = $(this);
					obj.val = opt.text();
					obj.index = opt.index();
					obj.placeholder.text(obj.val);
				});
			},
			getValue : function() {
				return this.val;
			},
			getIndex : function() {
				return this.index;
			}
		}
		$(function() {
			var dd = new DropDown( $('#dd') );
		});
    $(function() {
			var dd = new DropDown( $('#dd2') );
		});
    </script>

    <!-- Add / Remove -->
    <script>
    /* Variables */
    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {
    row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
    button.closest("div").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($("#participantTable div").length < 17) {
      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(".remove").on('click', function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
</body>
</html>
