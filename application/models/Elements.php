<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function ultimos_talleres(){		
		$this->db->limit(3);	
		$galeria = $this->db->get('footer_ultimos');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/'.$g->tipo.'/'.$g->foto);
			$galeria->row($n)->fecha = !empty($g->fecha) && $g->fecha!='0000-00-00'?strftime('%d %B %Y',strtotime($g->fecha)):'';	
			$galeria->row($n)->link = base_url($g->tipo.'/'.toUrl($g->id.'-'.$g->nombre));
			$galeria->row($n)->lugar = !empty($g->lugar)?'en '.$g->lugar:'';
		}
		return $galeria;
	}

	function talleres_main(){
		$this->db->limit(3);	
		$galeria = $this->db->get('talleres');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/talleres/'.$g->foto);
			$galeria->row($n)->fecha = strftime('%d %B %Y',strtotime($g->fecha));	
			$galeria->row($n)->descripcion = cortar_palabras(strip_tags($g->descripcion),20).'...';
			$galeria->row($n)->link = base_url('talleres/'.toUrl($g->id.'-'.$g->nombre));
			$galeria->row($n)->linkreserva = base_url('reservar/talleres/'.toUrl($g->id.'-'.$g->nombre));
		}
		return $galeria;
	}

	function servicios_main(){				
		$this->db->select('servicios.*, categorias_servicios.id as cat, categorias_servicios.nombre as nom');
		$this->db->join('categorias_servicios','categorias_servicios.id = servicios.categorias_servicios_id');
		$galeria = $this->db->get('servicios');
		foreach($galeria->result() as $n=>$g){
			$galeria->row($n)->foto = base_url('img/servicios/'.$g->foto);			
			$galeria->row($n)->descripcion = cortar_palabras(strip_tags($g->descripcion),20).'...';
			$galeria->row($n)->link = base_url('terapies/'.toUrl($g->cat.'-'.$g->nom).'/'.toUrl($g->id.'-'.$g->nombre));
		}
		return $galeria;
	}
}