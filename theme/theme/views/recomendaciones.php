<div id="slider-gris-01" class="contenedor-recomendaciones">
    <div class="row titulo-redeem texto-negro"><h2><b>Recomendaciones</b></h2></div>

    <div class="row slider-gris-01">
      <div class="col-sm-12">
      <!-- Tarjeta -->
      <?php
        if(empty($recomendaciones)){
            $this->db->limit(4);
            $this->db->order_by('id','DESC');
            $recomendaciones = $this->querys->get_listas();
        }
      ?>
      <?php foreach($recomendaciones->result() as $row): ?>
        <!-- Tarjeta -->
        <div class="col-sm-3">
            <?php get_instance()->load->view('views/_lista_item',array('lista'=>$row)); ?>
        </div>
      <?php endforeach ?>
    </div>


    </div>
</div>
