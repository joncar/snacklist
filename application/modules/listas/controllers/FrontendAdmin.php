<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class FrontendAdmin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = 'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $this->load->view($template,$param);
                }                
            }
        }

        public function listas(){
        	$this->norequireds = array('user_id');
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                 ->unset_edit()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->set_rules('user_id','USER','callback_validar_filas');
            $crud->callback_before_insert(function($post){
                $post['user_id'] = get_instance()->user->id;
                $post['fecha'] = date("Y-m-d H:i:s");
                return $post;
            });            
            $crud->callback_after_insert(function($post,$primary){
            	if(!empty($_POST['detalles']) && count($_POST['detalles'])>0){            		
            		$x = 0;
                    $puntos = 0;
                    $incremento = 10;
            		foreach($_POST['detalles'] as $n=>$v){
            			$data = $v;
            			$data['posicion'] = $x+1;
                        $data['votos'] = count($_POST['detalles'])-($data['posicion']-1);
                        $data['votos_originales'] = $data['votos'];
            			$data['listas_id'] = $primary;
            			$x++;
            			if(!empty($v['nombre'])){
            				$this->db->insert('listas_detalles',$data);
            				$this->db->insert('listas_detalles_original',$data);
                            $puntos+=$incremento;
                            $incremento-= $incremento>0?1:0;
        				}                        
            		}
                    //Sumar puntos a la lista
                    $this->db->update('listas',array('votos'=>$puntos),array('id'=>$primary));                    
            		if(!empty($_POST['categorias_id'])){
            			foreach($_POST['categorias_id'] as $n=>$c){
            				$data = array(
            					'listas_id'=>$primary,
            					'categorias_id'=>$_POST['categorias_id'][$n],
            					'subcategorias_id'=>$_POST['sub_categorias_id'][$n],
            					'original'=>0
            				);
            				$this->db->insert('listas_categorias',$data);
            				$data['original'] = 1;
            				$this->db->insert('listas_categorias',$data);
            			}
            		}

                    //Dar snacks por su creación
                    $this->querys->dar_snacks(6,array('url'=>'lista/'.toUrl($primary.'-'.$post['titulo_corto']),'user_id'=>$this->user->id,'lista'=>$post['titulo_corto']));
                    $this->querys->reordenarlista($primary);
            	}
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function editarLista(){
            $this->as['editarLista'] = 'listas';
            $this->norequireds = array('user_id');
            $crud = $this->crud_function('',''); 
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->set_rules('user_id','USER','callback_validar_filas');

            $crud->fields('user_id');
            $crud->callback_before_update(function($post,$primary){
                $lista = $this->db->get_where('listas',array('id'=>$primary))->row();
                $post['user_id'] = $lista->user_id;
                $post['veces_ordenado'] = $lista->veces_ordenado++;
                //Verificar si hay que eliminar imagen anterior
                if(strpos($lista->adjunto,'http') && file_exists('img/listas/'.$lista->adjunto)){
                    unlink('img/listas/'.$lista->adjunto);
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){
                $this->db->delete('listas_categorias',array('listas_id'=>$primary,'original'=>0));
                $this->db->delete('listas_detalles',array('listas_id'=>$primary));

                if(!empty($_POST['detalles']) && count($_POST['detalles'])>0){                  
                    $x = 0;
                    $puntos = 0;
                    $incremento = 10;
                    foreach($_POST['detalles'] as $n=>$v){
                        $data = $v;
                        $data['posicion'] = $x+1;
                        $data['votos'] = count($_POST['detalles'])-$data['posicion'];
                        $data['listas_id'] = $primary;
                        $x++;
                        if(!empty($v['nombre'])){
                            $this->db->insert('listas_detalles',$data);         
                            $puntos+=$incremento;
                            $incremento-= $incremento>0?1:0;                   
                        }                                            
                    }

                    //Sumar puntos
                    $puntos += $this->db->get_where('listas',array('id'=>$primary))->row()->votos;
                    $this->db->update('listas',array('votos'=>$puntos),array('id'=>$primary));
                    //Ha reordenado
                    $hasreordenado = $this->db->get_where('user',array('id'=>$this->user->id))->row()->has_reordenado+1;
                    $this->db->update('user',array('has_reordenado'=>$hasreordenado),array('id'=>$this->user->id));

                    if(!empty($_POST['categorias_id'])){
                        foreach($_POST['categorias_id'] as $n=>$c){
                            $data = array(
                                'listas_id'=>$primary,
                                'categorias_id'=>$_POST['categorias_id'][$n],
                                'subcategorias_id'=>@$_POST['sub_categorias_id'][$n],
                                'original'=>0
                            );
                            $this->db->insert('listas_categorias',$data);                            
                        }
                    }
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function validar_filas(){
            if(!empty($_POST) && !empty($_POST['detalles'])){
                $x = 0;
                foreach($_POST['detalles'] as $d){
                    if(!empty($d['nombre']) && !empty($d['autor'])){
                        $x++;
                    }
                }
                if($x>=3){
                    //Validamos que todos los enviados tengan nombre y autor
                    foreach($_POST['detalles'] as $d){
                        if(!empty($d['adjunto']) && (empty($d['autor']) || empty($d['nombre']))){
                            $this->form_validation->set_message('validar_filas','No esta permitido enviar una imagen sin nombre ni autor.');
                            return false;
                        }
                    }

                    if(count($_POST['categorias_id'])==0){
                        $this->form_validation->set_message('validar_filas','Debe elegir al menos una categoria.');
                        return false;   
                    }

                    foreach($_POST['categorias_id'] as $n=>$c){
                        if(empty($_POST['sub_categorias_id'][$n])){
                            $this->form_validation->set_message('validar_filas','Debe elegir una subcategoria.');
                            return false;   
                        }                        
                    }

                    return true;
                }
            }
            $this->form_validation->set_message('validar_filas','Una lista debe contener al menos 3 elementos');
            return false;
        }

        function mensajes(){            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('mensajes')
                 ->where('user_id',$this->user->id)
                 ->set_subject('mensajes')
                 ->set_theme('mensajes')
                 ->order_by('id','DESC')
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_jquery()                 
                 ->set_url('listas/frontendAdmin/mensajes/');            
            $crud = $crud->render('','theme/theme/views/listados/');
            $this->db->update('mensajes',array('leido'=>1),array('user_id'=>$this->user->id));
            $this->loadView(array('view'=>'views/mensajes','title'=>'Centro de mensajeria','js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));             
        }

        function perfil($x = '',$y = ''){
            
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('user')
                 ->set_theme('perfil')
                 ->set_subject('Perfil');
            $crud->field_type('admin','hidden')
                 ->field_type('status','hidden')
                 ->field_type('fecha_registro','hidden')
                 ->field_type('has_votado','invisible')
                 ->field_type('has_reordenado','invisible')
                 ->field_type('has_compartido','invisible')                
                 ->field_type('puntos','hidden');
            $crud->field_type('password','password')
                 ->field_type('password2','password')
                 ->display_as('password','Contraseña')
                 ->display_as('password2','Repetir Contraseña')
                 ->display_as('nickname','Usuario');
            if($crud->getParameters(FALSE)!='update'){
                $crud->fields('foto','nickname','password','password2','cp');
                if(!empty($_POST['password']) && empty($_POST['password2']) || !empty($_POST['password2']) && (empty($_POST['password']) || empty($_POST['password2']) || $_POST['password'] != $_POST['password2'])){
                    $crud->set_rules('password','Contraseña','required|matches[password2]');
                }
            }else{
                $crud->fields('foto','nickname','password','cp');
            }            
            $crud->set_primary_key_value($this->user->id);
            $crud->set_url('listas/frontendAdmin/perfil/');
            //$crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->set_field_upload('foto','img/fotos');
            $crud->set_lang_string('form_edit','<b>Guardar</b>');
            $crud->callback_field('genero',function($val){
                return $val;
            })->callback_field('password',function($val){
                return '<input type="password" name="password" id="field-password" class="form-control">';
            })->callback_field('password2',function($val){
                return '<input type="password" name="password2" id="field-password2" class="form-control">';
            });
            $crud->callback_before_update(function($post,$primary){
                $original = $this->db->get_where('user',array('id'=>$primary))->row()->password;
                if(empty($post['password'])){
                    $post['password'] = $original;
                }
                if(!empty($primary) && $original!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $crud->unset_back_to_list()->unset_list()->unset_add()->unset_delete()->unset_export()->unset_print()->unset_read();
            if(empty($x)){
                $crud = $crud->render(3,'theme/theme/views/listados/');
            }else{
                $crud = $crud->render('','theme/theme/views/listados/');
            }
            $this->loadView(array('view'=>'views/perfil','title'=>'Perfil de usuario','js_files'=>$crud->js_files,'css_files'=>$crud->css_files,'output'=>$crud->output));             
        }
        
    }
?>
