DROP VIEW IF EXISTS view_mis_listas;
CREATE VIEW view_mis_listas AS 
SELECT 
listas.id,
listas.fecha,
listas.titulo,
listas.titulo_corto,
categorias.id as catid,
categorias.nombre as categoria,
sub_categorias.nombre as subcategoria,
listas.vistas,
listas.votos,
listas.user_id
FROM listas_categorias
INNER JOIN listas ON listas.id = listas_categorias.listas_id
INNER JOIN categorias ON categorias.id = listas_categorias.categorias_id
INNER JOIN sub_categorias ON sub_categorias.id = listas_categorias.subcategorias_id
GROUP BY listas.id