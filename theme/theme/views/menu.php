




<div id="myHeader" class="visible-sm visible-md visible-lg" style=" background-color: black;">
    <nav class="navbar navbar-color-on-scroll navbar-transparent  fixed-top  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
        <div class="container-fluid">
            <div class="navbar-translate">
                <a href="<?= base_url() ?>" class="logo-header">
                    <img src="<?= base_url() ?>theme/theme/assets/img/logo-snacklist.png" alt="Logo Snacklist">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>


            <div class="collapse navbar-collapse js-navbar-collapse">

                <ul class="nav navbar-nav">
                    <li>
                        <form autocomplete="off">
                            <div class="autocomplete" id="buscador">
                                <input class="form-control" id="myInput" type="text" name="Prueba" placeholder="Buscar...">
                            </div>
                        </form>
                    </li>
                </ul>

                <ul class="nav navbar-nav">
                  <li class="dropdown mega-dropdown">

                    <a href="#" class="dropdown-toggle btn-menu-top-sesion" data-toggle="dropdown"><b style="color: black;">Categorias</b></a>

                    <ul class="dropdown-menu mega-dropdown-menu row" style="margin-left:0px; margin-right:0px;">
                        <div class="col-sm-12 btn-menu-categorias text-uppercase">
                            <a href="<?= base_url() ?>categorias"><i class="fa fa-eye" aria-hidden="true"></i> ver todas las categorías y subcategorías</a>
                        </div>

                        <div class="col-sm-12 btn-menu-categorias">
                            <div class="col-sm-2">
                                <li class="dropdown-header text-left">Destacadas</li>
                                <li class="divider"></li>
                                <?php
                                    $this->db->limit(5);
                                    $this->db->join('categorias','categorias.id = categorias_destacadas.categorias_id');
                                    foreach($this->db->get_where('categorias_destacadas')->result() as $c): ?>
                                    <li style="text-align: left; font-size:16px;"><a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>" style="font-size:16px;"><?= $c->nombre ?></a></li>
                                <?php endforeach ?>
                            </div>

                            <div class="col-sm-10 orden-alfabetico-menu">
                                <?php
                                    $this->db->limit(6);
                                    $letras = $this->db->query('SELECT SUBSTRING(nombre,1,1) as Inicial from categorias GROUP BY SUBSTRING(nombre,1,1) ORDER BY SUBSTRING(nombre,1,1) ASC');
                                    foreach($letras->result() as $l):
                                ?>
                                    <div class="col-sm-2">
                                        <div>
                                          <div class="dropdown-header todas-categorias"><b><?= $l->Inicial ?></b></div>
                                          <?php
                                              $this->db->limit(3);
                                              foreach($this->db->get_where('categorias',array('SUBSTRING(nombre,1,1)'=>$l->Inicial))->result() as $c):
                                          ?>
                                          <div style="margin-bottom:5px;">
                                              <a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>">-<?= $c->nombre ?></a>
                                          </div>
                                          <?php endforeach ?>
                                          <div class="divider"></div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>

                    </ul>

                  </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item">
                        <a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-facebook-official"></i></a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa fa-twitter"></i></a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-google-plus-official"></i></a>
                    </li>

                    <li class="nav-item"><a href="<?= base_url() ?>publicar-lista" class="listado-slider-bottom btn-listado-slider-top btn-general center-block"><b style="font-size: 16px;">Publica tu lista</b></a></li>

                    <?php if(empty($this->user->id)): ?>
                        <li class="dropdown nav-item">
                            <a href="#" class="dropdown-toggle btn-menu-top-sesion" data-toggle="dropdown"><b style="color: black;">Iniciar sesión</b></a>
                            <div class="dropdown-menu dropdown-menu-right"  id="dropdown-login">
                                <div class="row fondo-iniciar-sesion text-center">
                                  <form action="<?= base_url('main/loginUser') ?>" method="post" onsubmit="return loginUser(this)">
                                        <div class="col-sm-12"><b>Iniciar sesión</b></div>
                                        <div class="col-sm-12">
                                            <input class="form-control margin-login-input" name="email" id="prueba" placeholder="Correo" type="email">
                                            <input class="form-control margin-login-input" name="pass" id="prueba" placeholder="Contraseña" type="password">
                                        </div>
                                        <div class="col-sm-12" style="color:black;">
                                            <button type="submit" class="login-btn">Iniciar sesión</button>
                                        </div>
                                        <div class="col-sm-12 recuperar-cuenta" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#recuperar-contraseña">
                                            <a href="#"><b>Olvidé contraseña</b></a>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="login-fb">
                                                <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
                                                    <i class="fa fa-facebook-official"></i> Ingresar con Facebook
                                                </fb:login-button>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" style="color:black;" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#login">
                                            <button type="button" class="login-btn" style="width: 100%;border: 0px;">Registrarme</button>
                                        </div>
                                        <div class="col-sm-12" id="responseLogin"></div>

                                        <!--
                                        <div class="col-sm-12">
                                            <div class="login-cuenta" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#login">
                                                <a href="#"><b>Registrarme</b></a>
                                            </div>
                                        </div>-->
                                  </form>
                              </div>
                            </div>
                        </li>
                    <?php else: ?>
                      <?php $this->load->view('views/menu-interno'); ?>
                    <?php endif ?>
                </ul>

            </div>
        </div>
    </nav>
</div>






<div id="myHeader" style=" background-color: black;">
    <nav class="navbar navbar-expand-lg navbar-dark visible-xs" id="menuResponsive">
      <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/theme/assets/img/logo-snacklist.png" alt="Logo Snacklist" style="width:50%;"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent" style="background-color: #000000;">

        <ul class="navbar-nav mr-auto">

          <li style="margin-bottom: 20px">
              <form autocomplete="off" action="/action_page.php">
                  <div class="autocomplete" id="buscador" style="width: 100%;">
                      <input class="form-control" id="myInput" type="text" name="Prueba" placeholder="Buscar...">
                  </div>
              </form>
          </li>

          <li class="nav-item btn-movil-menu">
            <a href="<?= base_url() ?>publicar-lista" style="color: white;">Publica tu lista</a>
          </li>

          <?php if(empty($this->user->id)): ?>
                <li class="nav-item btn-movil-menu">
                    <a href="#" data-original-title="" data-toggle="modal" data-target="#iniciar-sesion" style="color: white;">Iniciar sesión</a>
                </li>

                <li class="nav-item btn-movil-menu">
                    <a href="#" data-original-title="" data-toggle="modal" data-target="#login" style="color: white;">Registrarme</a>
                </li>

                <li class="dropdown mega-dropdown btn-movil-menu">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:white; margin-top:3px; width: 100%;">Categorias</a>

                <ul class="dropdown-menu mega-dropdown-menu row" style="margin-left:0px; margin-right:0px;">
                    <div class="col-sm-12 btn-menu-categorias text-uppercase">
                        <a href="<?= base_url() ?>categorias"><i class="fa fa-eye" aria-hidden="true"></i> Ver todas las categorías</a>
                    </div>

                    <div class="col-sm-12 btn-menu-categorias">
                        <div class="col-sm-2">
                            <li class="dropdown-header text-left">Destacadas</li>
                            <li class="divider"></li>
                            <?php
                                $this->db->limit(5);
                                $this->db->join('categorias','categorias.id = categorias_destacadas.categorias_id');
                                foreach($this->db->get_where('categorias_destacadas')->result() as $c): ?>
                                <li style="text-align: left; font-size:16px;"><a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>" style="font-size:16px;"><?= $c->nombre ?></a></li>
                            <?php endforeach ?>
                        </div>

                        <div class="col-sm-10 orden-alfabetico-menu">
                            <?php
                                $this->db->limit(6);
                                $letras = $this->db->query('SELECT SUBSTRING(nombre,1,1) as Inicial from categorias GROUP BY SUBSTRING(nombre,1,1) ORDER BY SUBSTRING(nombre,1,1) ASC');
                                foreach($letras->result() as $l):
                            ?>
                                <div class="col-sm-2">
                                    <div>
                                      <div class="dropdown-header todas-categorias"><b><?= $l->Inicial ?></b></div>
                                      <?php
                                          $this->db->limit(3);
                                          foreach($this->db->get_where('categorias',array('SUBSTRING(nombre,1,1)'=>$l->Inicial))->result() as $c):
                                      ?>
                                      <div style="margin-bottom:5px;">
                                          <a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>">-<?= $c->nombre ?></a>
                                      </div>
                                      <?php endforeach ?>
                                      <div class="divider"></div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>

                </ul>

              </li>


            <?php else: ?>
                <li class="nav-item btn-movil-menu">
                    <a href="<?= base_url('mensajeria') ?>" style="color: white;">
                        Notificaciones
                    </a>
                </li>
                <li class="nav-item btn-movil-menu">
                    <a href="<?= base_url('mis-listas') ?>" style="color: white;">
                        Mis listas
                    </a>
                </li>
                <li class="nav-item btn-movil-menu">
                    <a href="<?= base_url('snacks') ?>" style="color: white;">
                        Mis Snacks
                    </a>
                </li>
                <li class="nav-item btn-movil-menu">
                    <a href="<?= base_url('perfil') ?>" style="color: white;">
                        Perfil
                    </a>
                </li>
                <li class="nav-item btn-movil-menu">
                    <a href="<?= base_url('main/unlog') ?>" style="color: white;">
                        Salir
                    </a>
                </li>
            <?php endif ?>
        </ul>
      </div>
    </nav>
</div>







<script>
jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
})

function loginUser(f){
  f = new FormData(f);
  $("#responseLogin").html('');
  remoteConnection('main/loginUser',f,function(data){
    data = JSON.parse(data);
    if(data.success){
      document.location.reload();
    }else{
      $("#responseLogin").html(data.message);
    }
  });
  return false;
}
</script>


<!-- Autocomplete -->
<script>
function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {

      var a, b, i, val = this.value;
      var t = this;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;

      $.post(URL+'listas/listados/buscador/json_list',{'busqueda':val},function(data){
        data = JSON.parse(data);
        arr = data;
        if(data.length===0){
          arr = [
            {titulo:'No se encontraron resultados',url:''}
          ];
        }
        a = document.createElement("DIV");
        a.setAttribute("id", t.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        t.parentNode.appendChild(a);
        /*make the matching letters bold:*/
        for(var i in arr){
            b = document.createElement("DIV");
            var text = arr[i].titulo;
            b.innerHTML = "<strong>" + text.substr(0, val.length) + "</strong>";
            b.innerHTML += text.substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='"+arr[i].url+"'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                //inp.value = this.getElementsByTagName("input")[0].value;
                var url = this.getElementsByTagName("input")[0].value;
                if(url!==''){
                  document.location.href=url;
                }
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
        }

      });


  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);
</script>
