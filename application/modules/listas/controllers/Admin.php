<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function listas_detalles($x = ''){
            $crud = $this->crud_function('','');             
            $crud->where('listas_id',$x)
                 ->set_field_upload('adjunto','files/listas')
                 ->field_type('listas_id','hidden',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function listas_detalles_original($x = ''){
            $crud = $this->crud_function('','');             
            $crud->where('listas_id',$x)
                 ->set_field_upload('adjunto','files/listas')
                 ->field_type('listas_id','hidden',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function listas_categorias($x = ''){
            $crud = $this->crud_function('','');             
            $crud->where('listas_id',$x)
                 ->field_type('listas_id','hidden',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function listas(){
            $crud = $this->crud_function('',''); 
            $crud->display_as('votos','Puntos')
                 ->display_as('user_id','Creador')
                 ->display_as('fecha','Fecha de creación')
                 ->columns('id','titulo','titulo_corto','user_id','votos','veces_ordenado','veces_compartido','vistas','fecha')
                 ->order_by('id','DESC')
                 ->callback_after_delete(function($primary){
                    get_instance()->db->delete('listas_categorias',['listas_id'=>$primary]);
                    get_instance()->db->delete('listas_detalles',['listas_id'=>$primary]);
                    get_instance()->db->delete('listas_detalles_original',['listas_id'=>$primary]);
                    get_instance()->db->delete('listas_trends',['listas_id'=>$primary]);
                });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function denuncias(){
            $crud = $this->crud_function('','');             
            $crud->field_type('estado','dropdown',array('0'=>'Pendiente','1'=>'Atendiendo','2'=>'Resuelto'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
