<div class="contenedor-snacktrend text-center">
    <div class="row logo-snacktrend">
        <div class="col-sm-12">
          <a href="<?= base_url('snacktrend') ?>">
            <img src="<?= base_url() ?>theme/theme/assets/img/logo-snacktrend.png" alt="Logo Snacktrend" class="center-block img-responsive">
          </a>
        </div>
    </div>

    <?php        
        $trends = $this->db->query('CALL getTrends()');
        @mysqli_next_result( $this->db->conn_id );
    ?>
    <!-- TOP 4 -->
    <div class="row texto-blanco padding30-bottom grid">
        
        <?php if($trends->num_rows()>0): ?>
          <?php 
            foreach($trends->result() as $i=>$t): 
            $t = $this->querys->get_listas(['listas.id'=>$t->listas_id])->row();
          ?>
              <div class="col-xs-6 <?php echo $i<4?'col-md-4 col-lg-3':'col-xs-6 col-md-4 col-lg-2' ?> texto-blanco">
                  <div class="contenedor-tarjeta-snacktrend">

                      <figure class="snip1585">
                        
                        <div class="img-snactrend-fondo" style="background:url(<?= $this->querys->get_foto_primaria($t->id) ?>); background-size:cover; width:100%; height:180px;">
                            <div class="titulo-header-tarjtea-snacktrend">
                                <div class="posicion-snacktrend"><?= $i+1 ?></div>
                                <div class="perfil-snacktrend" style="background:url(<?= $t->perfil ?>)"></div>
                                <div class="username-snacktrend"><?= $t->username ?><br><b>Votos:<?= $t->votos ?></b></div>
                            </div>
                            <img src="<?= $this->querys->get_foto_primaria($t->id) ?>" alt="<?= $t->titulo ?>" style="display:none;" />
                        </div>

                        <figcaption>
                            <h3 class="btns-votar-snacktrend">
                                <a href="<?= $t->link ?>" class="btn-ver-snacktrend-vistas"><i class="fa fa-eye" aria-hidden="true"></i> <?= $t->vistas ?></a>
                            </h3>
                            <h3 class="btns-votar-snacktrend">
                                <a href="<?= $t->link ?>" class="btn-ver-snacktrend">Votar/Reordenar</a>
                            </h3>
                        </figcaption>

                      </figure>

                      <div class="footer-tarjeta-snacktrend">
                          <h3>
                            <span class="categoria-snacktrend">
                              <?= $t->catnom ?> / <?= $t->subcatnom ?>
                            </span>
                            <br><a href="<?= $t->link ?>" style="color: white;"><?= $i<4?$t->titulo_corto:cortar_palabras($t->titulo_corto,4) ?></a>
                          </h3>
                      </div>
                  </div>
              </div>            
          <?php endforeach ?>
        <?php endif ?>
  </div>
</div>

<script>
/* Demo purposes only */
$(".hover").mouseleave(
  function() {
    $(this).removeClass("hover");
  }
);
</script>
