<div class="contenedor-footer-01">
    <div class="row mapa-sitio-footer">
        <div class="col-xs-12 col-sm-6">
            <div style="margin-bottom: 10px;">
                <h3><b>Categorías</b></h3>
            </div>
            <div style="display: flex">
                <div style="width:100%">
                    <ul class="navbar-nav mr-auto nav-footer">
                        <?php
                            $this->db->limit(11);
                            $this->db->order_by('nombre','ASC');
                            foreach($this->db->get_where('categorias')->result() as $c):
                        ?>
                            <a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>"><li class="nav-item"><?= $c->nombre ?></li></a>
                        <?php endforeach ?>
                    </ul>
                </div>
                <div style="width:100%">
                    <ul class="navbar-nav mr-auto nav-footer">
                        <?php
                            $this->db->limit(11,11);
                            $this->db->order_by('nombre','ASC');
                            foreach($this->db->get_where('categorias')->result() as $c):
                        ?>
                            <a href="<?= base_url('categoria/'.toUrl($c->id.'-'.$c->nombre)) ?>"><li class="nav-item"><?= $c->nombre ?></li></a>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="contenedor-snacktrend-footer">
                <div class="row logo-snacktrend-footer">
                    <div class="col-xs-12 col-sm-12 text-center">
                        <div class="col-xs-12 col-sm-6">
                            <img src="<?= base_url() ?>theme/theme/assets/img/logo_snacktrend_footer.png" alt="Logo Snacktrend" class="img-responsive center-block">
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <a href="<?= base_url() ?>snacktrend"><i class="fa fa-trophy" aria-hidden="true"></i> <b>Ver Ranking</b></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <?php
                            $this->db->limit(5);
                            $this->db->group_by('id');
                            $this->db->order_by('votos','DESC');
                            $this->db->order_by('fecha_trend','ASC');
                            foreach($this->db->get_where('view_ranking')->result() as $n=>$s):
                        ?>
                            <div class="titulo-footer-snacktrend">
                                <div class="posicion-snacktrend-footer"><?= $n+1 ?></div>
                                <a href="<?= base_url('lista/'.toUrl($s->id.'-'.$s->titulo_corto)) ?>">
                                    <div class="lista-footer"><?= $s->titulo_corto ?>...</div>
                                </a>
                            </div>
                        <?php endforeach ?>

                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <?php
                            $this->db->limit(5,5);
                            $this->db->group_by('id');
                            foreach($this->db->get_where('view_ranking')->result() as $n=>$s):
                        ?>
                            <div class="titulo-footer-snacktrend">
                                <div class="posicion-snacktrend-footer"><?= $n+6 ?></div>
                                <a href="<?= base_url('lista/'.toUrl($s->id.'-'.$s->titulo_corto)) ?>">
                                    <div class="lista-footer"><?= $s->titulo_corto ?>...</div>
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
