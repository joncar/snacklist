<div id="slider-categorias">
    <div class="owl-carousel owl-theme slider-categorias">
          <a href="#"><div class="item item-slider-categorias"><h4>Deportes</h4></div></a>
          <a href="#"><div class="item item-slider-categorias02"><h4>Deportes</h4></div></a>
          <a href="#"><div class="item item-slider-categorias03"><h4>Deportes</h4></div></a>
          <a href="#"><div class="item item-slider-categorias"><h4>Deportes</h4></div></a>
          <a href="#"><div class="item item-slider-categorias02"><h4>Deportes</h4></div></a>
          <a href="#"><div class="item item-slider-categorias03"><h4>Deportes</h4></div></a>
    </div>

    <script>
        var owl = $('.slider-categorias');
        owl.owlCarousel({
          items:7,
          loop:true,
          margin:10,
          autoplay:true,
          autoplayTimeout:2000,
          autoplayHoverPause:true
        });
        $('.play').on('click',function(){
          owl.trigger('play.owl.autoplay',[2000])
        })
        $('.stop').on('click',function(){
          owl.trigger('stop.owl.autoplay')
        })
    </script>
</div>
