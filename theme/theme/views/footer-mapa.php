<div class="row">
    <div class="col-xs-12 col-sm-4 logo-footer">
        <a href="<?= base_url() ?>p/index.php"><img src="<?= base_url() ?>theme/theme/assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive center-block"></a>
        Snacklist <?= date("Y") ?> | Todos los derechos reservados
    </div>
    <div class="col-xs-12 col-sm-6 text-center margin-mapa-footer hidden-xs">
        <ul class="navbar-nav mr-auto nav-mapa-sitio">
            <a href="<?= base_url() ?>"><li class="nav-item"><b>Inicio</b></li></a> |
            <a href="<?= base_url() ?>terminos.html"><li class="nav-item"><b>Términos y condiciones</b></li></a> |
            <a href="<?= base_url() ?>aviso.html"><li class="nav-item"><b>Aviso de Privacidad</b></li></a> |
            <a href="<?= base_url() ?>preguntas.html"><li class="nav-item"><b>Preguntas Frecuentes</b></li></a> |
            <a href="<?= base_url() ?>contacto.html"><li class="nav-item"><b>Contáctanos</b></li></a>
            
            
            
        </ul>
    </div>

    <div class="col-xs-12 col-sm-6 text-center margin-mapa-footer visible-xs">
            <a href="<?= base_url() ?>"><b>Inicio</b></a><br>
            <a href="<?= base_url() ?>terminos.html"><b>Términos y condiciones</b></a><br>
            <a href="<?= base_url() ?>aviso.html"><b>Aviso de Privacidad</b></a><br>
            <a href="<?= base_url() ?>preguntas.html"><b>Preguntas Frecuentes</b></a><br>
            <a href="<?= base_url() ?>contacto.html"><b>Contáctanos</b></a><br>
            
            
            

            <a href="#" class="btn btn-just-icon btn-link"><i class="fa fa-facebook-square"></i></a>
            <a href="#" class="btn btn-just-icon btn-link"><i class="fa fa-google-plus-official"></i></a>
    </div>

    <div class="col-xs-12 col-sm-2 hidden-xs">
        <ul class="social-buttons">
            <li>
                <a href="#" class="btn btn-just-icon btn-link"><i class="fa fa-facebook-square"></i></a>
            </li>
            <li>
                <a href="#" class="btn btn-just-icon btn-link"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="#" class="btn btn-just-icon btn-link"><i class="fa fa-google-plus-official"></i></a>
            </li>
        </ul>
    </div>
</div>


