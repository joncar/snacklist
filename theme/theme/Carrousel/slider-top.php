<!-- Item 01 -->
<div class="item background-cover fondo-slider01">
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1>¿Cuáles son los mejores restaurantes en CDMX?</h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <div class="row">
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">2. Morimoto</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">5. Pujol</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">10. Suntory</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom btn-listado-slider-top btn-general"><a href="">Ver Lista</a></div>
            </div>
        </div>
    </div>
</div>

<!-- Item 02 -->
<div class="item background-cover fondo-slider02">
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1>¿Cuáles son las mejores frutas?</h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <div class="row">
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">2. Fresa</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">5. Sandía</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">10. Fruta</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom btn-listado-slider-top btn-general"><a href="">Ver Lista</a></div>
            </div>
        </div>
    </div>
</div>

<!-- Item 03 -->
<div class="item background-cover fondo-slider02">
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1>¿Cuáles son los mejores estadios?</h1></div>
    </div>
    <div class="container-fluid lista-slider-top texto-blanco">
        <div class="row">
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">2. Fresa</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">5. Sandía</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom"><a href="">10. Fruta</a></div>
            </div>
            <div class="col-md-3">
                <div class="listado-slider-bottom btn-listado-slider-top btn-general"><a href="">Ver Lista</a></div>
            </div>
        </div>
    </div>
</div>
