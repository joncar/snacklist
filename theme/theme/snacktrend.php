<div class="contenedor-snacktrend text-center">
    <div class="row logo-snacktrend">
        <img src="assets/img/logo-snacktrend.png" alt="Logo Snacktrend" class="center-block img-responsive">
    </div>

    <!-- TOP 4 -->
    <div class="row texto-blanco padding30-bottom grid">
        <div class="col-md-3 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista01.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend">
                    <h3><span class="categoria-snacktrend">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-3 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista02.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend">
                    <h3><span class="categoria-snacktrend">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-3 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista03.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend">
                    <h3><span class="categoria-snacktrend">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-3 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista04.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend">
                    <h3><span class="categoria-snacktrend">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>
    </div>

    <!-- TOP 6 -->
    <div class="row texto-blanco">
        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista04.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista05.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista06.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista04.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista05.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>

        <div class="col-md-2 texto-blanco">
            <div class="contenedor-tarjeta-snacktrend">
                <figure class="snip1585">
                  <div class="titulo-header-tarjtea-snacktrend">
                      <div class="posicion-snacktrend">3</div>
                      <div class="perfil-snacktrend"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                      <div class="username-snacktrend">Username 911<br><b>Votos:718</b></div>
                  </div>
                  <img src="assets/img/Tarjetas/lista06.jpg" alt="sample70" />
                  <figcaption>
                      <h3>
                          <button class="btn-ver-snacktrend"><a href="#">Votar</a></button>
                          <button class="btn-ver-snacktrend"><a href="#">Reordenar</a></button>
                      </h3>
                      <h3 class="vistas-snacktrend">
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i> 100</a>
                          <a href="#"><i class="fa fa-list" aria-hidden="true"></i>Ver</a>
                      </h3>
                  </figcaption>
                  <a href="#"></a>
                </figure>
                <div class="footer-tarjeta-snacktrend-top6">
                    <h3><span class="categoria-snacktrend-top6">/Música y entretenimiento</span><br>Mejores canciones rockeras de todos los tiempos</h3>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
/* Demo purposes only */
$(".hover").mouseleave(
  function() {
    $(this).removeClass("hover");
  }
);
</script>
