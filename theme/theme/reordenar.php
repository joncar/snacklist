<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <div class="container-fluid contenedor-snackteam fondo-blanco">
        <div class="container">
            <div class="row text-center contenedor-perfil-redeem">
                <img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                <h3><b>Snackteam</b></h3>
            </div>
            <div class="row text-center contenedor-votos-redeem">
                <div class="col-sm-3 borde-redeem"><b>39 Vistas</b></div>
                <div class="col-sm-3 borde-redeem"><b>72 Votos</b></div>
                <div class="col-sm-3 borde-redeem"><b>4 Reordenadas</b></div>
                <div class="col-sm-3">
                    <div class="texto-comparte-redeem"><b>Comparte esta lista:</b></div>
                    <div class="redes-redeem">
                        <a href="#"><i class="fa fa-facebook-official"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 padding0 breadcumb-redeem">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#"><b>Categoría</b></a></li>
                        <li class="breadcrumb-item"><a href="#"><b>Moda y Belleza</b></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><b>Eventos</b></li>
                      </ol>
                    </nav>
                </div>
                <div class="col-sm-6 padding0 reportar-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#reportar-lista">
                    <a href="#"><i class="material-icons">flag</i><b>Reportar Lista</b></a>
                </div>
            </div>
            <div class="row titulo-redeem texto-negro"><h2>Teorías y leyes científicas que todos deberíamos conocer.</h2></div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12 col-md-12">
                      <ul class="nav nav-pills contenedor-nav-redeem padding0">
                          <li class="nav-item"><a class="nav-link active show" href="#reordenar" data-toggle="tab"><b>Reordenar</b></a></li>
                          <li class="nav-item"><a class="nav-link" href="#votar" data-toggle="tab"><b>Votar</b></a></li>
                      </ul>
                      <div class="tab-content tab-space">
                          <!-- Contenedor Votar -->
                          <div class="tab-pane active show" id="reordenar">
                              <div class="col-sm-12 texto-intrucciones">
                                  <p>
                                    <b>Reordenar</b><br>
                                    Con la acción de reordenar puedes sumar hasta 55 puntos a la lista existente. Cada vez que reordenas
                                    una lista de 10 elementos, sumas un total de 55 puntos a la lista. Estos puntos se suman a los ya acumulados
                                    para determinar la posición del elemento.
                                  </p>
                              </div>
                              <!-- TOP 10 -->
                              <div class="col-sm-12 contenedor-tabs-redeem fondo-azul-claro">

                                <div class='parent'>
                                  <div class='wrapper'>
                                    <div id='left-rm-spill' class='container-drag'>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                1<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                2<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                3<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                4<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                5<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <div class="col-sm-12 contenedor-ads">
                                            <div class="container">
                                                <div class="row text-center">Google ADS</div>
                                            </div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                6<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                7<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                8<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                9<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                        <!-- Tarjeta redeem -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem">
                                            <div class="col-sm-1 numero-votar">
                                                <i class="material-icons">arrow_drop_up</i><br>
                                                10<br>
                                                <i class="material-icons">arrow_drop_down</i>
                                            </div>
                                            <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                            <div class="col-sm-7 titulo-reddem">
                                                <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                            </div>
                                            <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                        </div>

                                </div><!-- Termina div Drag & Drop -->
                              </div>
                            </div>

                              </div>

                              <!-- Posiciones -->
                              <div class="col-sm-12 contenedor-tabs-redeem">
                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-votar">
                                          <i class="material-icons">arrow_drop_up</i><br>
                                          11<br>
                                          <i class="material-icons">arrow_drop_down</i>
                                      </div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-votar">
                                          <i class="material-icons">arrow_drop_up</i><br>
                                          12<br>
                                          <i class="material-icons">arrow_drop_down</i>
                                      </div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-votar">
                                          <i class="material-icons">arrow_drop_up</i><br>
                                          13<br>
                                          <i class="material-icons">arrow_drop_down</i>
                                      </div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2 icon-redeem"><i class="material-icons">more_vert</i></div>
                                  </div>

                              </div>

                              <div class="col-sm-4 col-sm-offstet-4 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Guardar</b></div>

                              <!-- Recomendaciones -->
                              <div class="col-sm-12 fondo-azul-claro margin-recomendaciones">
                                  <?php include('recomendaciones.php');?>
                              </div>

                          </div>

                          <!-- Contenedor Reordenar -->
                          <div class="tab-pane" id="votar">
                              <div class="col-sm-12 texto-intrucciones">
                                  <p>
                                    <b>Votar</b><br>
                                    Con la acción de votar das click en +1 o -1 en cada elemento que quieras. Asi los elementos pueden
                                    cambiar su posición dentro de la lista. Si coincide la sumatoria de dos elementos, el que estaba
                                    arriba prevalece hasta arriba.
                                  </p>
                              </div>
                              <!-- TOP 10 -->
                              <div class="col-sm-12 contenedor-tabs-redeem fondo-azul-claro">
                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">1</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-menos">-1</div>
                                          <div class="reordenar-redeem numero-mas">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">2</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-menos">-1</div>
                                          <div class="reordenar-redeem numero-mas">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">3</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">4</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">5</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">6</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">7</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">8</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">9</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">10</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-bloqueado">-1</div>
                                          <div class="reordenar-redeem numero-bloqueado">+1</div>
                                      </div>
                                  </div>

                              </div>

                              <!-- Posiciones -->
                              <div class="col-sm-12 contenedor-tabs-redeem">
                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">11</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-menos">-1</div>
                                          <div class="reordenar-redeem numero-mas">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">12</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-menos">-1</div>
                                          <div class="reordenar-redeem numero-mas">+1</div>
                                      </div>
                                  </div>

                                  <!-- Tarjeta redeem -->
                                  <div class="col-sm-12 contenedor-tarjeta-redeem">
                                      <div class="col-sm-1 numero-reordenar">13</div>
                                      <div class="col-sm-2"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem"></div>
                                      <div class="col-sm-7 titulo-reddem">
                                          <h2><b>La ley de Newton de la gravitación universal</b><br><span>Foto: wikipedia.com</span></h2>
                                      </div>
                                      <div class="col-sm-2">
                                          <div class="reordenar-redeem numero-menos">-1</div>
                                          <div class="reordenar-redeem numero-mas">+1</div>
                                      </div>
                                  </div>

                              </div>

                          </div>
                      </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

</body>
</html>
