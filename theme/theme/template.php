<!doctype html>
<html lang="en">

	<!-- Google Web Fonts
	================================================== -->

	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i%7CFrank+Ruhl+Libre:300,400,500,700,900" rel="stylesheet">

	<!-- Basic Page Needs
	================================================== -->

	<title><?= empty($title) ? 'Monalco' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': $description ?>" /> 	
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">
	<?php if(!empty($foto)): ?>		
		<meta property="og:image" content="<?= $foto ?>" />
		<meta property="og:description" content="The Snackilst: Listas creadas y votadas por todos." />
	<?php endif ?>
	<meta property="fb:app_id" content="518978775229184" />	
	<script>var URL = '<?= base_url() ?>';</script>

	<?php $this->load->view('views/head') ?>

    <?php if(!empty($css_files)): foreach($css_files as $file): ?>
    		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
	<?php endforeach; ?>
    <?php endif; ?>
</head>

<body>
	 <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php $this->load->view('views/menu.php');?>
    </div>

	<!-- Body Wrapper -->
	<?php 
		if(empty($editor)){
			$this->load->view($view); 
		}else{
			echo $view;
		}
	?>	
</body>
</html>