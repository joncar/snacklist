<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Maestras extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function categorias(){
            $crud = $this->crud_function('',''); 
            $crud->set_field_upload('icono','img/categorias');           
            $crud->field_type('foto','image',array('path'=>'img/categorias','width'=>'1300px','height'=>'400px')); 
            $crud->field_type('banner','image',array('path'=>'img/categorias','width'=>'1300px','height'=>'443px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function sub_categorias(){
            $crud = $this->crud_function('','');            
            $crud->set_field_upload('icono','img/categorias');           
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function tipos_snacks(){
            $crud = $this->crud_function('','');             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function categorias_home(){
            $crud = $this->crud_function('','');             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function categorias_destacadas(){
            $crud = $this->crud_function('','');             
            $crud = $crud->render();
            $this->loadView($crud);
        }

        public function sliders(){
            $crud = $this->crud_function('','');    
            $crud->field_type('foto','image',array('path'=>'img/sliders','width'=>'1300px','height'=>'443px'));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
    }
?>
