<div class="container-fluid fondo-gradient-azul contenedor-preguntas">
    <div class="container fondo-preguntas">

        <div class="row text-center superior-perfil">
            <div class="col-xs-12 col-md-12 text-center"><h2><b>Contacto</b></h2></div>
        </div>

        <div class="row contenedor-datos-editar">

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="contenedor-seccion-informativa">

                    <div class="col-sm-12 form-contacto">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Nombre" maxlength="8" type="text">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Teléfono" maxlength="8" type="tel">
                        <textarea class="form-control" id="textarea" placeholder="Mensaje" name="textarea" rows="10" cols="50"></textarea>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 text-center btn-menu-top btn-general btn-derecha pull-right center-block">
                        <b>Enviar comentarios</b>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
