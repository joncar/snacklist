<?php    
class Traduccion{
    public function traducir($view,$idioma = ''){   
        $this->db = get_instance()->db;     
        $traducciones = $this->db->get_where('traducciones',array('idioma'=>$idioma));
        $lang = array();
        if($traducciones->num_rows()>0){
            foreach($traducciones->result() as $v){
                $lang[$v->original] = $v->traduccion;
            }
        }


        $view = str_replace('data-','s1s1w3',$view);
        $view = str_replace('data=','s1s1w4',$view);
        $view = str_replace('(data)','s1s1w5',$view);
        if(!empty($lang)){
            foreach($lang as $n=>$v){                
                $view = str_replace($n,$v,$view);
            }
        }
        $view = str_replace('s1s1w3','data-',$view);
        $view = str_replace('s1s1w4','data=',$view);
        $view = str_replace('s1s1w5','(data)',$view);
        $view = $this->traducirbd($view);
        return $view;
    }

    function traducirbd($view){
        //Reemplazar inputs
        //$_SESSION['lang'] = 'ca';
        $this->db = get_instance()->db;
        $dbname = $this->db->database;
        $tables = $this->db->query("SELECT t.TABLE_NAME AS name FROM INFORMATION_SCHEMA.TABLES as t WHERE t.TABLE_SCHEMA = '$dbname'");
        $lang = $_SESSION['lang'];
        if($lang!='ca'){
            foreach($tables->result() as $table){
                $fields = $this->db->field_data($table->name);
                foreach($fields as $field){
                    if($field->name=='idiomas'){
                        $values = $this->db->get($table->name);
                        foreach($values->result() as $v){
                            if(!empty($v->idiomas)){
                                $idiomas = json_decode($v->idiomas);
                                foreach($v as $n=>$vv){
                                    if($n!='id' && !is_numeric($vv)){
                                        if(!empty($idiomas->{$lang}->{$n})){
                                            $repl = $idiomas->{$lang}->{$n};
                                            $view = str_replace($vv,$repl,$view);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $view;
    }
}    
