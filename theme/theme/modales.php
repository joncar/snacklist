<!-- Modal Foto Publicar Lista -->
<div class="modal fade" id="seleccionar-foto-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Subir imagen</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>Subir una imagen para la lista</b></div>
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <span class="btn btn-file">
                            <span class="fileinput-new">
                                <div class="center-block" id="img-login-registro"><i class="material-icons">camera_alt</i></div>
                            </span>
                            <span class="fileinput-exists">Change</span>
                            <input name="..." type="file">
                        </span>
                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Guardar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Foto Publicar Lista -->

<!-- Modal Video Publicar Lista -->
<div class="modal fade" id="seleccionar-video-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Subir video</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>Selecciona un video</b></div>
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <span class="btn btn-file">
                            <span class="fileinput-new">
                                <div class="center-block" id="img-login-registro"><i class="material-icons">play_arrow</i></div>
                            </span>
                            <span class="fileinput-exists">Change</span>
                            <input name="..." type="file">
                        </span>
                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Guardar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Video Publicar Lista -->

<!-- Modal Gif Publicar Lista -->
<div class="modal fade" id="seleccionar-gif-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Subir video</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>Selecciona un gif</b></div>
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <span class="btn btn-file">
                            <span class="fileinput-new">
                                <div class="center-block" id="img-login-registro"><i class="material-icons">gif</i></div>
                            </span>
                            <span class="fileinput-exists">Change</span>
                            <input name="..." type="file">
                        </span>
                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Guardar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Gif Publicar Lista -->

<!-- Modal Eliminar  Lista -->
<div class="modal fade" id="eliminar-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Eliminar Lista</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>¿Seguro de eliminar esta lista?</b></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Eliminar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Eliminar Lista -->

<!-- Modal Reportar Lista -->
<div class="modal fade" id="reportar-lista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Denunciar anónimamente</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div><b>Si deseas denunciar este comentario de forma anónima, por favor indícanos la razón y lo revisaremos personalmente.</b></div>
                    <div class="reportar-modal">
                        <div class="form-check margin-bottom-modal">
                            <label class="form-check-label">
                                <input class="form-check-input" name="exampleRadios" id="exampleRadios1" value="option1" type="radio"> El contenido es inapropiado, explícito, publicitario u ofensivo
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check margin-bottom-modal">
                            <label class="form-check-label">
                                <input class="form-check-input" name="exampleRadios" id="exampleRadios2" value="option2" checked="" type="radio">El contenido es deliberadamente malintencionado
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="form-check margin-bottom-modal">
                            <label class="form-check-label">
                                <input class="form-check-input" name="exampleRadios" id="exampleRadios2" value="option2" checked="" type="radio">El contenido es falso o contiene información falsa
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Enviar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Reportar Lista -->

<!-- Modal Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Regístrate</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">

                    <div class="col-sm-12">
                        <div><b>Crea una cuenta</b></div>
                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <span class="btn btn-file">
                                <span class="fileinput-new">
                                    <div class="center-block" id="img-login-registro"><i class="material-icons">camera_alt</i></div>
                                </span>
                                <span class="fileinput-exists">Change</span>
                                <input name="..." type="file">
                            </span>
                            <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Nickname" type="text" maxlength="8">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Contraseña" type="password" maxlength="8">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Confirmar contraseña" type="password" maxlength="8">

                        <div class="col-sm-12 genero-login">
                            <div class="col-sm-6 form-check margin-bottom-modal">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" id="exampleRadios1" value="option1" type="radio">Soy Hombre
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="col-sm-6 form-check margin-bottom-modal">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="exampleRadios" id="exampleRadios2" value="option2" checked="" type="radio">Soy Mujer
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>

                        <input class="form-control margin-login-input datepicker" id="prueba" value="10/05/2016" placeholder="Fecha de cumpleaños" style="" type="text">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Código Postal" type="tel" maxlength="8">
                    </div>
                    <div class="col-sm-12 texto-aviso-login">
                      *Al crear una cuenta aceptas el uso de cookies, los <a href="#">Términos y condiciones</a> y el <a href="#">Aviso de privacidad.</a>
                    </div>
                    <div class="col-sm-12 captcha">Aquí va el captcha</div>
                    <div class="col-sm-12">
                        <div class="col-sm-12 padding0"><a href="publicar-lista.php" class="btn btn-primary" style="width:100%; margin-bottom:10px;"><b>Crear cuenta</b></a></div>
                        <div class="col-sm-12"><a href="#">Ya tengo cuenta</a></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Login -->

<!-- Modal Login -->
<div class="modal fade" id="recuperar-contraseña" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Recupera tu contraseña</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 center-block text-center">
                    <div class="col-sm-12">
                        <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6"><a href="#" class="btn btn-primary"><b>Ya tengo cuenta</b></a></div>
                        <div class="col-sm-6"><a href="#" class="btn btn-primary"><b>Recuperar mi contraseña</b></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Login -->





<!-- Modal Listas VS Listas -->
<div class="modal fade" id="listavslista" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="row text-center titulo-modal center-block">Denunciar anónimamente</div>
        <div class="modal-content">
            <div class="modal-header center-block text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="material-icons">clear</i></button>
                <h5 class="modal-title"><img src="assets/img/logo-snacklist-black.png" alt="Logo Snacktrend" class="img-responsive"></h5>
            </div>
            <div class="modal-body">


                <div class="ontenedor-snackteam fondo-blanco" id="menu-categorias">


                         <div class="row margin-titulo-slider-home">
                            <h2 class="titulo-gastronomia texto-blanco">
                              <a href="http://www.labtico.com/categoria/21-shopping"><img src="http://www.labtico.com/img/categorias/18919-icono-entretenimiento.png">Shopping</a>
                            </h2>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <!--- Original ---->
                                <div class="col-xs-12 col-sm-6 lista-original">

                                    <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 16</div>
                                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                                        <b>Mi lista original</b><br>
                                                <a href="http://www.labtico.com/categoria/21-shopping"><span class="categoria-snacktrend">Shopping</span></a>
                                                                </div>
                                    <a href="http://www.labtico.com/lista/5-juguetes-de-ni-os">
                                        <div class="texto-gris-oscuro">
                                            <h3><b>Juguetes de niños</b></h3>
                                        </div>
                                                                        <div class="contenedor-listado-categoria">
                                                <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcSC3_ASNZIYCF7LMWzWghBCRvgcn_tpN2vtRSyRXWOalmEWZnobfmpgO70'); background-size:cover; background-position: center center;">
                                                    <div class="top-listado-tarjeta texto-blanco">1</div>
                                                </div>
                                                <div class="listado-tarjeta"><b>Bicicleta</b><br>Yo</div>
                                            </div>
                                                                        <div class="contenedor-listado-categoria">
                                                <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcTJNv5aAH3zAfQqi3S8pLCptqnX9ZBGWH67Nw6v8Lsp-blDSDeoZfV7JRM'); background-size:cover; background-position: center center;">
                                                    <div class="top-listado-tarjeta texto-blanco">2</div>
                                                </div>
                                                <div class="listado-tarjeta"><b>Monopatines</b><br>Yo</div>
                                            </div>
                                                                        <div class="contenedor-listado-categoria">
                                                <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcRwG85mPBVMZmC89yP28juUlcQAH9o3y5wRDc_pCSr5u_LVat78JzooHs5s'); background-size:cover; background-position: center center;">
                                                    <div class="top-listado-tarjeta texto-blanco">3</div>
                                                </div>
                                                <div class="listado-tarjeta"><b>Patines</b><br>Patines</div>
                                            </div>
                                                                </a>
                                    <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                                        <div class="perfil-tarjeta-categorias">
                                            <img src="http://www.labtico.com/img/fotos/36640-logo-snacklist.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                                        </div>
                                        <div class="username-tarjetas"><b>Username root</b><br>Votos:54</div>
                                    </div>

                                </div>
                                <!--Final ------->

                                <div class="col-xs-12 col-sm-6">
                                    <div class="margin-listas-comparacion">
                                        <div class="vistas-tarjetas icon-vistas">
                                              <i class="fa fa-eye" aria-hidden="true"></i> 54                            </div>
                                        <div class="header-tarjeta-categoria texto-gris-oscuro">
                                            <b>Última versión</b><br>
                                                                                <a href="http://www.labtico.com/categoria/21-shopping">
                                                  <span class="categoria-snacktrend">Shopping</span>
                                                </a>
                                                                        </div>
                                        <a href="http://www.labtico.com/lista/5-juguetes-de-ni-os">
                                            <div class="texto-gris-oscuro">
                                                <h3><b>Juguetes de niños</b></h3>
                                            </div>
                                            <div class="">
                                                                                    <div class="contenedor-listado-categoria ">
                                                    <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcSC3_ASNZIYCF7LMWzWghBCRvgcn_tpN2vtRSyRXWOalmEWZnobfmpgO70'); background-size:cover; background-position: center center;">
                                                      <div class="top-listado-tarjeta texto-blanco">1</div>
                                                    </div>
                                                    <div class="listado-tarjeta"><b>Bicicleta</b><br>Yo</div>
                                                </div>
                                                                                    <div class="contenedor-listado-categoria ">
                                                    <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcTJNv5aAH3zAfQqi3S8pLCptqnX9ZBGWH67Nw6v8Lsp-blDSDeoZfV7JRM'); background-size:cover; background-position: center center;">
                                                      <div class="top-listado-tarjeta texto-blanco">2</div>
                                                    </div>
                                                    <div class="listado-tarjeta"><b>Monopatines</b><br>Yo</div>
                                                </div>
                                                                                    <div class="contenedor-listado-categoria ">
                                                    <div class="imagen-listado-tarjeta" style="background:url('http://t0.gstatic.com/images?q=tbn:ANd9GcRwG85mPBVMZmC89yP28juUlcQAH9o3y5wRDc_pCSr5u_LVat78JzooHs5s'); background-size:cover; background-position: center center;">
                                                      <div class="top-listado-tarjeta texto-blanco">3</div>
                                                    </div>
                                                    <div class="listado-tarjeta"><b>Patines</b><br>Patines</div>
                                                </div>
                                                
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!--Fin comparación ---->
                            </div>
                        </div>

                    </div>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><b>Cancelar</b></button>
                <button type="button" class="btn btn-success"><b>Enviar</b></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Listas VS Listas -->
