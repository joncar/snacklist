
    <div class="container-fluid fondo-gradient-azul contenedor-preguntas">
        <div class="container fondo-preguntas">
            <div class="row titulo-seccion">
                <div class="col-md-12 text-center"><h2><b>Términos y Condiciones</b></h2></div>
            </div>

            <div class="row text-center fondo-gris">
                <div class="col-md-12">

                    <div class="preguntas">
                        <?= $this->db->get('ajustes')->row()->aviso_legal ?>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
