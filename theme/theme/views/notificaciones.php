
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 contenedor-ads">Google ADS</div>
            </div>
        </div>
    </div>

    <div class="container-fluid fondo-gradient-azul contenedor-preguntas">
        <div class="container fondo-preguntas padding0">
            <div class="row titulo-seccion">
                <div class="col-md-12 text-center"><h2><b>Notificaciones</b></h2></div>
            </div>

            <div class="contendor-fondo-notificaciones">
                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b>Jorge Torres</b> votó -1 en tu lista <big><b>Los mejores Celulares del 2018</b></big><br>
                          <small class="texto-azul-light">Hace 2 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+20 Puntos</b> publicaste <big><b>Los peores nombres para perros</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+10 Puntos</b> publicaste <big><b>Los mejores películas animadas de todos los tiempos</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+20 Puntos</b> publicaste <big><b>Top Canciones</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+1 Punto</b> publicaste <big><b>Los peores raps en español</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+1 Punto</b> publicaste <big><b>Los mejores marcadores</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->

                <!-- Notificacion -->
                <div class="row text-center">
                    <div class="col-md-12 contenedor-notificacion sin-borde-notificacion">
                        <div class="imagen-notificacion">
                            <img src="<?= base_url() ?>theme/theme/assets/img/Iconos/sesion.png" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">
                        </div>
                        <div class="texto-notificacion">
                          <b class="texto-azul-light">+10 Puntos</b> SrTachenko reordenó <big><b>Los mejores destinos cercanos a CDMX</b></big><br>
                          <small class="texto-azul-light">Hace 5 horas</small>
                        </div>
                    </div>
                </div>
                <!-- Notificacion -->
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Add / Remove -->
    <script>
    /* Variables */
