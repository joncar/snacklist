<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function view_usuarios($action = '',$return = ''){
        	$this->as['view_usuarios'] = 'view_reportes_usuarios';
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()
        		 ->unset_edit()
        		 ->unset_delete()
        		 ->unset_print()
        		 ->unset_export()
        		 ->unset_read();
        	if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
        		$desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
        		$hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
        		$crud->where('DATE(fecha) >=',$desde);
        		$crud->where('DATE(fecha) <=',$hasta);
        	}
            $crud->unset_searchs('nivel');
            $crud->columns('id','fecha','nombre','status','contrasena','cumpleanos','genero','cp','nivel','snacks');
            $crud->callback_column('nivel',function($val,$row){
                $nivel = $this->querys->get_nivel($row->id);
                return $nivel['nivel'];
            });
            $crud->callback_column('snacks',function($val,$row){
                $nivel = $this->querys->get_puntos($row->id);
                return $nivel->total;
            });
            $crud->add_action('Listas','',base_url('reportes/admin/listas_usuarios').'/');
            $crud->add_action('Votos','',base_url('reportes/admin/votos_usuarios').'/');
            $crud->add_action('Shares','',base_url('reportes/admin/shares_usuarios').'/');
            $crud->add_action('reordenados','',base_url('reportes/admin/reordenados_usuarios').'/');
            $crud->add_action('Niveles','',base_url('reportes/admin/niveles_log').'/');
            $crud->add_action('Snacks','',base_url('reportes/admin/view_snacks').'/');
            if($return==1){
                return $crud;
            }
        	$crud = $crud->render();
        	$crud->crud = 'fechas';
        	$crud = $this->loadView($crud);           	     	
        }

        function view_reportes_listas($action = '',$return = ''){        	
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()
        		 ->unset_edit()
        		 ->unset_delete()
        		 ->unset_print()
        		 ->unset_export()
        		 ->unset_read();
        	if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
        		$desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
        		$hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
        		$crud->where('DATE(fecha) >=',$desde);
        		$crud->where('DATE(fecha) <=',$hasta);
        	}
        	$crud->add_action('Elementos','',base_url('reportes/admin/view_reportes_listas_detalles').'/');
        	$crud->add_action('Votos','',base_url('reportes/admin/view_reportes_listas_votos').'/');
            $crud->add_action('Reordenados','',base_url('reportes/admin/view_reportes_listas_reordenar').'/');
            $crud->add_action('Shares','',base_url('reportes/admin/view_reportes_listas_shares').'/');
        	if($return){
        		return $crud;	
        	}
        	$crud = $crud->render();
        	$crud->crud = 'fechas';
        	$crud = $this->loadView($crud);  
        }

        function view_reportes_listas_detalles($id = ''){        	
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()
        		 ->unset_edit()
        		 ->unset_delete()
        		 ->unset_print()
        		 ->unset_export()
        		 ->unset_read()
        		 ->unset_columns('listas_id','id','autor');
        	$crud->where('listas_id',$id);
        	if(!empty($_POST['desde']) && !empty($_POST['hasta'])){
        		$desde = date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde'])));
        		$hasta = date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta'])));
        		$crud->where('DATE(fecha) >=',$desde);
        		$crud->where('DATE(fecha) <=',$hasta);
        	}

            $crud->callback_column('adjunto',function($val,$row){
                return '<img src="'.$this->querys->get_foto($val).'">';
            });
        	
        	$crud = $crud->render();        	

        	$this->as['view_reportes_listas_detalles'] = 'view_reportes_listas';
        	$header = $this->view_reportes_listas('',1);
        	$header->set_theme('header_data');
        	$header->where('id',$id);
        	$crud->header = $header->render(1)->output;
        	
        	$this->loadView($crud);  
        }

        function view_reportes_listas_votos($id = ''){  
            $this->as['view_reportes_listas_votos'] = 'listas_log';
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_columns('listas_id','id','autor');
            $crud->where('j5aff5cc6.listas_id',$id);      
            $crud->where('descripcion',1);
            $crud->set_relation('listas_detalles_id','listas_detalles','nombre');              
            $crud->field_type('descripcion','dropdown',array('1'=>'Voto','2'=>'Reordenado'));
            $crud = $crud->render();            

            $this->as['view_reportes_listas_votos'] = 'view_reportes_listas';
            $header = $this->view_reportes_listas('',1);
            $header->set_theme('header_data');
            $header->where('id',$id);
            $crud->header = $header->render(1)->output;
            
            $this->loadView($crud);  
        }

        function view_reportes_listas_reordenar($id = ''){  
            $this->as['view_reportes_listas_reordenar'] = 'listas_log';
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_columns('listas_id','id','autor');
            $crud->where('j5aff5cc6.listas_id',$id);  
            $crud->where('descripcion',2);    
            $crud->set_relation('listas_detalles_id','listas_detalles','nombre');              
            $crud->field_type('descripcion','dropdown',array('1'=>'Voto','2'=>'Reordenado'));
            $crud = $crud->render();            

            $this->as['view_reportes_listas_reordenar'] = 'view_reportes_listas';
            $header = $this->view_reportes_listas('',1);
            $header->set_theme('header_data');
            $header->where('id',$id);
            $crud->header = $header->render(1)->output;
            
            $this->loadView($crud);  
        }

        function view_reportes_listas_shares($id = ''){  
            $this->as['view_reportes_listas_shares'] = 'shares';
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_columns('listas_id','id','autor');
            $crud->where('shares.listas_id',$id);                          
            $crud->field_type('red','dropdown',array('1'=>'Twitter','2'=>'Facebook'));
            $crud = $crud->render();            

            $this->as['view_reportes_listas_shares'] = 'view_reportes_listas';
            $header = $this->view_reportes_listas('',1);
            $header->set_theme('header_data');
            $header->where('id',$id);
            $crud->header = $header->render(1)->output;
            
            $this->loadView($crud);  
        }

        function view_reportes_snacktrends($id = ''){  
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('view_ranking')
                 ->set_subject('bootstrap2')
                 ->set_theme('bootstrap2')
                 ->set_primary_key('id')
                 ->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_edit()
                 ->group_by('view_ranking.id')
                 ->order_by('votos','DESC')
                 ->columns('id','fecha','username','titulo','catnom','votos','veces_ordenado')
                 ->set_relation('user_id','user','{nickname}|{foto}')                 
                 ->callback_column('username',function($val,$row){
                    list($nick,$perfil) = explode('|',$row->{'s'.substr(md5('user_id'),0,8)});
                    return '<img src="'.$this->querys->get_perfil($perfil).'" style="width:50px"> '.$nick;
                 })
                 ->callback_column('titulo',function($val,$row){
                    return '<a href="'.base_url('lista/'.toUrl($row->id.'-'.$val)).'">'.$val.'</a>';
                 })
                 ->display_as('user_id','Autor')
                 ->display_as('veces_ordenado','Reordenada')
                 ->display_as('catnom','Categoria');

            $crud = $crud->render();                        
            $this->loadView($crud);
        }

        function reordenados_usuarios ($id){
            $this->as['reordenados_usuarios'] = 'listas_log';
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();
            $crud->display_as('listas_detalles_id','Elemento')
                 ->display_as('sfb1fbeb4','Lista');
            $crud->set_relation('listas_detalles_id','listas_detalles','nombre')
                 ->set_relation('j5aff5cc6.listas_id','listas','titulo_corto');
            $crud->columns('sfb1fbeb4','listas_detalles_id','voto','fecha');
            $crud->where('listas_log.user_id',$id)
                 ->where('listas_log.descripcion',2);
            $crud = $crud->render();

            $this->as['reordenados_usuarios'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);

        }
        function shares_usuarios ($id){
            $this->as['shares_usuarios'] = 'shares';
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();
            $crud->where('shares.user_id',$id)
                 ->field_type('red','dropdown',array('2'=>'Facebook','1'=>'Twitter'));
            $crud = $crud->render();

            $this->as['shares_usuarios'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);

        }
        function listas_usuarios ($id){
            $this->as['listas_usuarios'] = 'listas';
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();
            $crud->where('user_id',$id);
            $crud = $crud->render();

            $this->as['listas_usuarios'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);

        }
        function votos_usuarios($id){
            $this->as['votos_usuarios'] = 'listas_log';
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();
            $crud->display_as('listas_detalles_id','Elemento')
                 ->display_as('sfb1fbeb4','Lista');
            $crud->set_relation('listas_detalles_id','listas_detalles','nombre')
                 ->set_relation('j5aff5cc6.listas_id','listas','titulo_corto');
            $crud->columns('sfb1fbeb4','listas_detalles_id','voto','fecha');
            $crud->where('listas_log.user_id',$id)
                 ->where('listas_log.descripcion',1);
            $crud = $crud->render();

            $this->as['votos_usuarios'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);

        }

        function niveles_log($id){
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();            
            $crud->where('niveles_log.user_id',$id);
            $crud = $crud->render();

            $this->as['niveles_log'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);
        }
        function view_snacks($id){
            $this->id = $id;
            $crud = $this->crud_function('','');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read();    
            $crud->set_primary_key('user_id');
            $crud->columns('registro','listas','ediciones_terceros','te_han_compartido','votos_terceros','has_reordenado','has_compartido','has_votado','trends','total');
            $crud->callback_column('trends',function($val,$row){
                $puntos = $this->querys->get_puntos($this->id);
                return $puntos->trends;
            });
            $crud->callback_column('total',function($val,$row){            
                return $row->trends+$row->total;
            });
            $crud->where('view_snacks.user_id',$id);
            $crud = $crud->render();

            $this->as['view_snacks'] = 'view_reportes_usuarios';
            $header = $this->view_usuarios('',1);
            $header->set_theme('header_data')->where('view_reportes_usuarios.id',$id);            
            $crud->header = $header->render(1)->output;
            $this->loadView($crud);
        }
        
    }
?>
