<?php 
foreach ($list as $num_row =>$row): 
  
?>
<div class="col-sm-3 margin-bottom-listas-nuevas tarjetero">
  <?php get_instance()->load->view('views/_lista_item',array('lista'=>$row->lista->row())); ?>
</div>
<?php endforeach ?>
<?php if(count($list)==0): ?>
   <div class="col-sm-12 text-center">
        <big><b>En estos momentos no existen subcategorias para esta categoria</big></b>
    </div>

    <div class="col-sm-12 contenedor-tabs-redeem center-block">
        <a href="<?= base_url() ?>categorias">
            <button style="background-color:transparent; border:0px;"><div class="text-center btn-menu-top btn-general btn-derecha"><b>Regresar al listado</b></div></button>
        </a>
    </div>

<?php endif ?>
