<?php
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . 'listas/js/cookies.js');
$this->set_js($this->default_theme_path . 'listas/js/flexigrid.js');
$this->set_js($this->default_theme_path . 'listas/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'listas/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'listas/js/pagination.js');
/** Fancybox */
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" data-unique-hash="<?php echo $unique_hash; ?>">

    <!--
    <div class="row titulo-redeem texto-negro">
      <div class="col-sm-12">
          <h3><b>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                <li class="breadcrumb-item"><a href="#">Categoría</a></li>
                <li class="breadcrumb-item active" aria-current="page">Subcategoría</li>
              </ol>
            </nav>
          </b></h3>
      </div>
    </div><br><br>-->


    <div class="row titulo-redeem texto-negro">
      <div class="col-sm-12">
          <h3><b>Categorías y Subcategorías</b></h3>
      </div>
    </div>

    <?php if (!empty($list)): ?>
        <div class="row ajax_list">
            <?php echo $list_view?>
        </div>
    <?php else: ?>
        Sin datos para mostrar
    <?php endif; ?>


    <!--
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers pageContent" id="dynamic-table_paginate">
                <ul class="pagination">
                </ul>
            </div>
        </div>
    </div>-->




<input type="hidden" name="per_page" value="<?= get_instance()->db->get('categorias')->num_rows(); ?>" class="per_page">
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
</div><?php echo form_close() ?>
