<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <!-- Menu interno -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu-interno.php');?>
    </div>

    <div class="container-fluid fondo-gradient-azul contenedor-preguntas">
        <div class="container fondo-preguntas">
            <div class="row">
                <div class="col-md-12 text-center"><h2><b>Editar perfil</b></h2></div>
            </div>

            <div class="row text-center fondo-gris">
                <div class="col-md-12">
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <span class="btn btn-file">
                            <span class="fileinput-new">
                                <div class="center-block" id="img-login-registro"><i class="material-icons">camera_alt</i></div>
                            </span>
                            <span class="fileinput-exists">Change</span>
                            <input name="..." type="file">
                        </span>
                        <a href="#" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                    </div>
                </div>

                <div class="col-md-12 text-center puntos-perfil"><h2><b>2,872</b><br><span class="puntos-perfil-small">Puntos</span></h2></div>
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 15%"></div>
                    </div>
						    </div>
            </div>

            <div class="row text-center contenedor-datos-editar">
                <div class="col-md-12">
                    <div class="nombre-perfil-editar center-block">
                        <div class="imagen-editar"><img src="assets/img/Perfiles/icon-editar-perfil.jpg"></div>
                        <div class="imagen-editar">
                            <span class="nivel-editar">Nivel 3</span><br>
                            <span class="titulo-editar"><b>Burrito</span></b>
                        </div>
                    </div>
                </div>


                <div class="col-sm-6 col-sm-offset-3">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Nickname" maxlength="8" type="text">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Contraseña" maxlength="8" type="password">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Confirmar contraseña" maxlength="8" type="password">

                    <div class="col-sm-12 genero-login">
                        <div class="col-sm-6 form-check margin-bottom-modal">
                            <label class="form-check-label">
                                <input class="form-check-input" name="exampleRadios" id="exampleRadios1" value="option1" type="radio">Soy Hombre
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                        <div class="col-sm-6 form-check margin-bottom-modal">
                            <label class="form-check-label">
                                <input class="form-check-input" name="exampleRadios" id="exampleRadios2" value="option2" checked="" type="radio">Soy Mujer
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </div>

                    <input class="form-control margin-login-input datepicker" id="prueba" value="10/05/2016" placeholder="Fecha de cumpleaños" style="" type="text">
                    <input class="form-control margin-login-input" id="prueba" placeholder="Código Postal" maxlength="8" type="tel">
                </div>
                
                <div class="col-sm-4 col-sm-offstet-4 text-center btn-menu-top btn-general btn-derecha pull-right center-block">
                    <b>Guardar cambios</b>
                </div>
            </div>


        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
</body>
</html>
