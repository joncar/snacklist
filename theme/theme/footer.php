<div class="container contenedor-footer-01">
    <div class="row"><div class="col-md-12 text-center titulo-footer text-uppercase"><h2>Snacklist | Listas para votar</h2></div></div>
    <div class="row"><div class="col-md-12 text-left"><h3>Categorías</h3></div></div>
    <div class="row mapa-sitio-footer">
        <div class="col-xs-12 col-sm-3">
            <ul class="navbar-nav mr-auto nav-footer">
                <a href="#"><li class="nav-item">Animales y Mascotas</li></a>
                <a href="#"><li class="nav-item">Arte y Cultura</li></a>
                <a href="#"><li class="nav-item">Autos y Motores</li></a>
                <a href="#"><li class="nav-item">Deportes</li></a>
                <a href="#"><li class="nav-item">Entretenimiento y Humor</li></a>
                <a href="#"><li class="nav-item">Fitness y Salud</li></a>
                <a href="#"><li class="nav-item">Gastronomía y Restaurantes</li></a>
                <a href="#"><li class="nav-item">Interesante y Curioso</li></a>
                <a href="#"><li class="nav-item">Internet y Redes Sociales</li></a>
                <a href="#"><li class="nav-item">Literatura y Publicaciones Moda y Belleza</li></a>
                <a href="#"><li class="nav-item">Música</li></a>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-3">
            <ul class="navbar-nav mr-auto nav-footer">
                <a href="#"><li class="nav-item">Negocios y Emprendedores</li></a>
                <a href="#"><li class="nav-item">Noticias</li></a>
                <a href="#"><li class="nav-item">Otros</li></a>
                <a href="#"><li class="nav-item">Política</li></a>
                <a href="#"><li class="nav-item">Productos y Servicios</li></a>
                <a href="#"><li class="nav-item">Shopping</li></a>
                <a href="#"><li class="nav-item">Sociedad y Eventos</li></a>
                <a href="#"><li class="nav-item">Tecnología e Innovación</li></a>
                <a href="#"><li class="nav-item">TV y Películas</li></a>
                <a href="#"><li class="nav-item">Viajes</li></a>
                <a href="#"><li class="nav-item">Vida y Estilo</li></a>
                <a href="#"><li class="nav-item">Videojuegos</li></a>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="contenedor-snacktrend-footer">
                <div class="row logo-snacktrend-footer">
                    <img src="assets/img/logo_snacktrend_footer.png" alt="Logo Snacktrend" class="img-responsive">
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">1</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">2</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">3</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">4</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">5</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">6</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">7</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">8</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">9</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>

                        <div class="titulo-footer-snacktrend">
                            <div class="posicion-snacktrend-footer">10</div>
                            <a href="#">
                                <div class="lista-footer">Mejores canciones rockeras de todos los tiempos.</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
