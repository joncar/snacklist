<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view('views/'.$url,array('link'=>$url),TRUE),
                    'link'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                /*$page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);*/
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('telefono','Telefono','required');   
            $this->form_validation->set_rules('politicas','Politicas','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $_SESSION['msj'] = $this->error('Captcha introduit incorrectament');
                }else{
                    $this->enviarcorreo((object)$_POST,1,'info@naturainda.com');
                    $_SESSION['msj'] = $this->success('Gràcies per contactar-nos, en breu ens posarem en contacte amb vostè');
                }
            }else{                
               $_SESSION['msj'] = 'Si us plau completi les dades sol·licitades  <script>$("#guardar").attr("disabled",false); </script>';
            }

            echo json_encode(array('text'=>$_SESSION['msj']));
            unset($_SESSION['msj']);
        }
        
        function enviarCurriculum(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('mensaje','Comentario','required');
            //$this->form_validation->set_rules('curriculum','Curriculum','required');            
            if($this->form_validation->run() && !empty($_FILES['curriculum'])){
                get_instance()->load->library('mailer');
                get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
                $this->enviarcorreo((object)$_POST,2,'info@finques-sasi.com');
                //$_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                echo json_encode(array('success'=>TRUE,'message'=>'Gracias por contactarnos, en breve le contactaremos'));
            }else{
                //$_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
               echo json_encode(array('success'=>FALSE,'message'=>'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>'));
            }
            /*if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url('p/contacto'));
            }*/
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = 'Inscrit amb éxit';
                    $err = 'success';
                }else{
                    $_SESSION['msj2'] = 'Aquest correu ja està registrat';
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo json_encode(array('result'=>$err,'msg'=>$_SESSION['msj2']));
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }
        
        function pdf(){
            require_once APPPATH.'libraries/html2pdf/html2pdf.php';
            $papel = 'L';
            $orientacion = 'P';
            $html2pdf = new HTML2PDF();
            $html2pdf->setDefaultFont('raleway');
            $html2pdf->addFont("ralewayb","B");
            $html2pdf->addFont("titanone","");
            $menu = $this->db->get('menu_del_dia')->row();
            $html = $menu->pdf;
            foreach($menu as $n=>$v){
                $html = str_replace('['.$n.']', str_replace('&bull;','<br/> &bull;',strip_tags($v)),$html);
            }
            $html = str_replace('[precio]',$this->db->get('ajustes')->row()->precio_menu_dia.'€',$html);
            $html = $this->load->view('pdf',array('html'=>$html),TRUE);
            $html2pdf->writeHTML($html); 
            //echo $this->db->get('menu_del_dia')->row()->pdf;
            ob_end_clean();         
            $html2pdf->Output('Menu-del-dia-'.date("dmY").'.pdf');
        }

        function galeria(){
            $this->loadView(array('view'=>'galeria','title'=>'Galeria','page'=>'','link'=>'galeria'));
        }

        function galeria_equipo(){
            $this->loadView(array('view'=>'galeria','title'=>'Galeria de nuestro equipo','page'=>''));
        }

        function validar_politicas($crud){

            if($crud->getParameters(FALSE)=='insert_validation'){                
                if(empty($_POST['politicas'])){
                    echo '<textarea>'.json_encode(array('success'=>false,'error_message'=>'Debe aceptar las politicas de privacidad')).'</textarea>';
                    die();
                }
            }else{
                unset($_POST['politicas']);
            }
        }

         function concurso($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('concurso')
                 ->set_theme('bootstrap3')
                 ->set_subject('')                 
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit()
                 ->display_as('nombre','Nombre Completo')
                 ->display_as('email','Email')
                 ->display_as('telefono','Teléfono para comunicarte que eres el/la ganador')
                 ->display_as('instituto','Instituto donde estudias')
                 ->display_as('curso','Curso que estudias')
                 ->display_as('propuesta','Tu mejor respuesta a por qué quieres ir al MIF2019 con un acompañante en condición de Very Important People (V.I.P.)')                 
                 ->set_rules('email','Email','required|is_unique[concurso.email]|valid_email');
            $crud->set_url('paginas/frontend/concurso/');
            $crud->callback_field('curso',function($val){
                return form_dropdown('curso',array('1º Bachillerato'=>'1º Bachillerato','2º Bachillerato'=>'2º Bachillerato'),'id="field-curso"');
            })
            ->callback_after_insert(function($post,$primary){
                get_instance()->enviarcorreo((object)$post,7,'info@mallorcaislandfestival.com');
                get_instance()->enviarcorreo((object)$post,8);
            });
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'concurso',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Concurso MIF'
                )
            );
        }


        function contacta($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('contacta')
                 ->set_theme('bootstrap3')
                 ->set_subject('')                 
                 ->field_type('comentario','editor',array('type'=>'textarea'))
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit()
                 ->display_as('nombre','Nombre Completo')
                 ->display_as('telefono','Teléfono')
                 ->display_as('estudiantes','Nº de Estudiantes')
                 ->display_as('oferta','¿Como te ha llegado la oferta?')
                 ->field_type('oferta','set',array(
                    'Flaix FM'=>'Flaix FM',
                    'Radio FlaixBack'=>'Radio FlaixBack',
                    'Prensa'=>'Prensa',
                    'TV'=>'TV',
                    'Recomendación de un amigo'=>'Recomendación de un amigo',
                    'Discoteca'=>'Discoteca',
                    'Profesor'=>'Profesor',
                    'AMPA'=>'AMPA',
                    'Otros'=>'Otros'
                 ));
            
            $crud->set_url('paginas/frontend/contacta/');
            $crud->callback_after_insert(function($post,$primary){
                $post['oferta'] = implode(',',$post['oferta']);
                get_instance()->enviarcorreo((object)$post,9,'info@mallorcaislandfestival.com');                
            });
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'mas_informacion',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Pedir más información'
                )
            );
        }

        function curriculum($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $this->validar_politicas($crud);
            $crud->set_table('trabaja_con_nosotros')
                 ->set_theme('bootstrap3')
                 ->set_subject('')
                 ->set_field_upload('curriculum','files/curriculum')
                 ->set_field_upload('titulaciones','files/curriculum')
                 ->field_type('comentarios','editor',array('type'=>'textarea'))                 
                 ->unset_back_to_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->required_fields_array()
                 ->unset_jquery()
                 ->unset_edit();
            $crud->set_url('paginas/frontend/curriculum/');
            if(empty($x)){
                $crud = $crud->render(2);            
            }else{
                $crud = $crud->render();            
            }
            $page = $this->load->view($this->theme.'trabaja-con-nosotros',array(),TRUE);
            $page = str_replace('[output]',$crud->output,$page);
            $this->loadView(
                array(
                'view'=>'read',
                'js_files'=>$crud->js_files,

                'page'=>$page,                
                'title'=>'Trabaja con nosotros'
                )
            );
        }
    }
