
    <div class="container-fluid  fondo-blanco margin-secciones-menu-top">
        <div class="container contenedor-ads">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <div class="container-fluid contenedor-snackteam fondo-blanco">
        <form action="" onsubmit="return guardarLista(this)" method="post">
            <div class="container">

                <div class="row-padding">
                    <div class="row titulo-redeem texto-negro"><h2>Editar lista</h2></div>

                    <div class="row titulo-editar-lista">
                        <div class="col-sm-12 titulo-grande-lista" style="padding-left:0px; padding-right:0px;">
                            Título de la lista:
                            <h3><b><?= $lista->titulo ?></b></h3>
                        </div>
                        <div class="col-sm-12" style="padding-left:0px; padding-right:0px;">
                            Título corto de la lista:
                            <h3><b><?= $lista->titulo_corto ?></b></h3>
                        </div>
                    </div>

                    <div class="row titulo-redeem texto-negro"><h3><b>Clasificación</b></h3></div>

                    <div class="row contenedor-select-publicar">
                        <div class="col-sm-12 padding0" id="participantTable">

                            <?php foreach($lista->categorias->result() as $c): ?>
                                <div class="participantRow">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select id="selectbasic" name="categorias_id[]" class="form-control select-publicar categorias_id">
                                                <option value="" id="input-publicar">Categoría</option>
                                                <?php foreach($this->db->get_where('categorias',array())->result() as $b): ?>
                                                <option value="<?= $b->id ?>" <?= $b->id==$c->categorias_id?'selected':'' ?>><?= $b->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select id="selectbasic" name="sub_categorias_id[]" class="form-control select-publicar subcategorias_id">
                                                <option value="" id="input-publicar">Subcategoría</option>
                                                <?php foreach($this->db->get_where('sub_categorias',array())->result() as $b): ?>
                                                <option value="<?= $b->id ?>" <?= $b->id==$c->subcategorias_id?'selected':'' ?>><?= $b->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!--<button class="remove btn-remover-categoria" type="button"><b>Eliminar</b></button>-->
                                </div>
                            <?php endforeach ?>

                        </div>

                        <!--
                        <div class="col-sm-12">
                            <div id="addButtonRow"><button class="add btn-agregar-categoria" type="button"><b>Agregar</b></button></div>
                        </div>-->
                    </div>

                    <div class="row">
                        <div class="col-sm-12 contenedor-tabs-redeem">
                            <div class='parent'>
                              <div class='wrapper'>
                                <div id='reordenar'>

                                    <!-- Tarjeta publicar -->
                                    <?php for($i=0;$i<10;$i++): ?>
                                        <!-- Tarjeta publicar -->
                                        <div class="col-sm-12 contenedor-tarjeta-redeem <?= $i<3?'lista-primeros-lugares':'' ?>">
                                            <div class="col-sm-1 numero-votar" style="line-height: 0.3;">
                                                <i class="material-icons"></i><br>
                                                <span class="indice"><?= $i+1 ?></span><br>
                                                <i class="material-icons"></i>
                                            </div>

                                            <div class="col-sm-8">
                                                <div class="col-sm-12">
                                                    <input class="form-control margin-login-input" name="detalles[<?= $i ?>][nombre]" <?= $i<$lista->detalles->num_rows()?'readonly':'' ?> type="text" value="<?= !empty($lista->detalles->row($i)->nombre) && $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->nombre:'' ?>">
                                                </div>

                                                <div class="col-sm-12">
                                                    <input class="form-control margin-login-input autor" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" placeholder="Autor de la imagen" type="text" value="<?= !empty($lista->detalles->row($i)->autor) && $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->autor:'' ?>">                                                    
                                                    <!--<input type="hidden" name="detalles[<?= $i ?>][nombre]" value="<?= !empty($lista->detalles->row($i)->nombre) && $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->nombre:'' ?>">                                                    -->
                                                    <input type="hidden" id="adjunto<?= $i ?>" class="adjunto" name="detalles[<?= $i ?>][adjunto]" value="<?= $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->adjunto:'' ?>">
                                                    <input type="hidden" id="tipo_adjunto<?= $i ?>" class="tipo_adjunto" name="detalles[<?= $i ?>][tipo_adjunto]" value="<?= $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->tipo_adjunto:'' ?>">
                                                    <input type="hidden" class="tipo_adjunto" name="detalles[<?= $i ?>][id]" value="<?= $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->id:'' ?>">
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <div rel="tooltip" onclick="setInputImagenLista(this)" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista" title="Agregar una imagen, video o gif">
                                                    <img id="previewLista<?= $i ?>" src="<?php
                                                        if($i<$lista->detalles->num_rows() && !empty($lista->detalles->row($i)->adjunto)){
                                                            if(strpos($lista->detalles->row($i)->adjunto,'http')>-1){
                                                                echo $lista->detalles->row($i)->adjunto;
                                                            }else{
                                                                echo base_url().'img/listas/'.$lista->detalles->row($i)->adjunto;
                                                            }
                                                        }else{
                                                            echo base_url().'theme/theme/assets/img/Perfiles/perfil.jpg';
                                                        }
                                                    ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem">
                                                </div>
                                                <a href="#" class="adjuntoPreview" title="Preview">
                                                    <div class="ver-preview"><i class="material-icons">remove_red_eye</i></div>
                                                </a>
                                            </div>

                                            <div class="col-sm-1 icon-redeem">
                                                <!--<div class="iconos-publicar iconos-publicar-lista" title="Eliminar"><i class="material-icons">delete_forever</i></div>-->
                                                <!--<div class="iconos-publicar iconos-publicar-lista" title="Limpiar"><i class="material-icons">replay</i></div>-->
                                            </div>
                                        </div>
                                    <?php endfor ?>

                                </div><!-- Termina div Drag & Drop -->
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div id="result"></div>
                        </div>
                        <div class="col-sm-12">
                          <button type="submit" style="background-color:transparent; padding:0;"><div class="col-sm-4 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Guardar cambios</b></div></button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Select Input -->
    <script>

    function guardarLista(form){

        <?php if(empty($_SESSION['user']) || $this->user->id!=$lista->user_id): ?>
        var f = new FormData(form);
        insertarUser('listas/frontend/editarLista/update/<?= $lista->id ?>',f,function(data){
            if(data.success){
                $("#result").addClass('alert alert-success').html('Se ha almacenado su lista con éxito');
            }
        });
        <?php else: ?>
        var f = new FormData(form);
        insertar('listas/frontendAdmin/editarLista/update/<?= $lista->id ?>',f,function(data){
            if(data.success){
                $("#result").addClass('alert alert-success').html('Se ha almacenado su lista con éxito');
            }
        });
        <?php endif ?>



        return false;
    }

    function ordenarIndex(){
        var x = 0;
        $("#reordenar .contenedor-tarjeta-redeem").each(function(){
            $(this).find('input').each(function(){
                var name = $(this).data('name');
                $(this).attr('name','detalles['+x+']['+name+']');
            });
            $(this).find('.indice').html(x+1);
            console.log(x);
            x++;
        })
      }


    function DropDown(el) {
			this.dd = el;
			this.placeholder = this.dd.children('span');
			this.opts = this.dd.find('.dropdown a');
			this.val = '';
			this.index = -1;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;
				obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					return false;
				});
				obj.opts.on('click',function(){
					var opt = $(this);
					obj.val = opt.text();
					obj.index = opt.index();
					obj.placeholder.text(obj.val);
				});
			},
			getValue : function() {
				return this.val;
			},
			getIndex : function() {
				return this.index;
			}
		}
		$(function() {
			var dd = new DropDown( $('#dd') );
		});
    $(function() {
			var dd = new DropDown( $('#dd2') );
		});
    </script>

    <!-- Add / Remove -->
    <script>
    var votos = JSON.parse(localStorage.votos);
    for(var i in votos){
        if($(".votar"+votos[i]).length>0){
            $(".votar"+votos[i]).remove();
        }
    }
    $(".votaciones").show();

    /* Variables */
    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {
    row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
    button.closest("div").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($("#participantTable div").length < 17) {
      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(".remove").on('click', function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
