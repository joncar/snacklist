<div class="row">
    <div class="col-xs-12 col-sm-4">
        <a href="index.php"><img src="assets/img/logo_snacktrend_footer.png" alt="Logo Snacktrend" class="img-responsive center-block"></a>
        Snacklist 2018 | Todos los derechos reservados
    </div>
    <div class="col-xs-12 col-sm-6 text-center margin-mapa-footer">
        <ul class="navbar-nav mr-auto nav-mapa-sitio">
            <a href="index.php"><li class="nav-item"><b>Inicio</b></li></a> |
            <a href="contacto.php"><li class="nav-item"><b>Contáctanos</b></li></a> |
            <a href="preguntas.php"><li class="nav-item"><b>Preguntas Frecuentes</b></li></a> |
            <a href="privacidad.php"><li class="nav-item"><b>Aviso de Privacidad</b></li></a> |
            <a href="terminos.php"><li class="nav-item"><b>Términos y condiciones</b></li></a>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-2">
        <ul class="social-buttons">
            <li><a href="#" target="_blank" class="btn btn-just-icon btn-link"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" target="_blank" class="btn btn-just-icon btn-link"><i class="fa fa-facebook-square"></i></a></li>
            <li><a href="#" target="_blank" class="btn btn-just-icon btn-link"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
    </div>
</div>
