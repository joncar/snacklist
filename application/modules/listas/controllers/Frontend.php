<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }

        public function publicar($x = ''){
            $this->loadView(array('view'=>'views/publicar-lista','title'=>'Publicar lista'));
        }

        public function listas_subcategorias(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('sub_categorias')                                  
                 ->set_theme('bootstrap2')
                 ->display_as('nombre','nombre')
                 ->display_as('categorias_id','categorias_id')
                 ->display_as('id','id')
                 ->columns('id','nombre','categorias_id','icono')
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export()
                 ->unset_jquery()
                 ->order_by('nombre','ASC');
            $crud = $crud->render('');            
        }

        function comparador($id){                    
            if(is_numeric($id)){
                if(empty($this->user->id)){
                    echo 'Debes iniciar sesión para poder ver esta información';
                    die();
                }
                $this->db->select('listas.*, user.nickname as username, user.foto as perfil');
                $this->db->join('user','user.id = listas.user_id');
                $lista = $this->db->get_where('listas',array('listas.id'=>$id,'user_id'=>$this->user->id));
                if($lista->num_rows()>0){    
                                    
                    $lista = $lista->row();                    
                    $this->db->where('original',0);
                    $this->db->select('categorias.*');                    
                    $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                    $lista->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$lista->id));
                    
                    $this->db->order_by('posicion','ASC');
                    $lista->detalles = $this->db->get_where('listas_detalles',array('listas_id'=>$lista->id));
                    

                    $original = clone $lista;
                    //original                                    
                    $this->db->where('original',1);
                    $this->db->select('categorias.*');                    
                    $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                    $original->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$original->id));
                    
                    $this->db->order_by('posicion','ASC');
                    $original->detalles = $this->db->get_where('listas_detalles_original',array('listas_id'=>$original->id));
                    

                    $this->load->view('views/comparador',array(
                        
                        'lista'=>$lista,
                        'original'=>$original,                        
                    ));
                }
            }
        }

        public function listas($categoria = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            
            if(!is_numeric($categoria)){
                $output = array();

                $this->db->select('categorias.id,categorias.nombre,listas.user_id');
                $this->db->join('listas','listas.id = listas_categorias.listas_id');
                $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                $this->db->group_by('categorias.id');
                $miscategorias = $this->db->get_where('listas_categorias',array('user_id'=>$this->user->id));

                foreach($miscategorias->result() as $m=>$c){
                    $crud = new ajax_grocery_crud();
                    $crud->set_table('view_mis_listas')
                         ->where('user_id ',$this->user->id)
                         ->where('catid',$c->id)
                         ->set_subject('listas')
                         ->set_theme('listas')                         
                         ->set_primary_key('id')
                         ->unset_add()
                         ->unset_edit()
                         ->unset_delete()
                         ->unset_read()
                         ->unset_print()
                         ->unset_export()
                         ->unset_jquery()
                         ->columns('id','catid','titulo','titulo_corto','vistas','votos','categoria','subcategoria','link','user_id')
                         ->set_url('listas/frontend/listas/'.$c->id.'/');            

                    $crud = $crud->render('','theme/theme/views/listados/');  
                    $miscategorias->row($m)->output = $crud->output;                  
                }

                if($miscategorias->num_rows()==0){
                    $crud = (object)array('js_files'=>'','css_files'=>'');
                }

                $this->loadView(array('miscategorias'=>$miscategorias,'view'=>'views/listas','title'=>'Mis listas','js_files'=>$crud->js_files,'css_files'=>$crud->css_files));


            }else{
                $crud = new ajax_grocery_crud();
                $crud->set_table('view_mis_listas')
                     ->where('user_id ',$this->user->id)
                     ->where('catid',$categoria)
                     ->set_subject('listas')
                     ->set_theme('listas')
                     ->set_primary_key('id')
                     ->columns('id','catid','titulo','titulo_corto','vistas','votos','categoria','link','user_id');                
                $crud = $crud->render('','theme/theme/views/listados/');       
                echo $crud->output;
            }            
        }

        public function ver($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $lista = $this->db->get_where('listas',array('id'=>$id));
                if($lista->num_rows()>0){                    
                    $lista = $lista->row();
                    //Sumar 1 a las vistas
                    if(!empty($_SESSION['user']) && $this->user->id == $lista->user_id){
                        redirect('editar-lista/'.toUrl($lista->id.'-'.$lista->titulo_corto));
                    }
                    $_SESSION['vistas'] = empty($_SESSION['vistas'])?array():$_SESSION['vistas'];
                    if(!in_array($lista->id,$_SESSION['vistas']) && (empty($_SESSION['user']) || $_SESSION['user']!=$lista->user_id)){                        
                        $_SESSION['vistas'][] = $lista->id;

                        $this->db->update('listas',array('vistas'=>($lista->vistas+1)),array('id'=>$id));
                    }
                    $this->db->where('original',0);
                    $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                    $lista->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$lista->id));

                    $this->db->where('original',0);
                    $this->db->join('sub_categorias','sub_categorias.id = listas_categorias.subcategorias_id');
                    $lista->subcategorias = $this->db->get_where('listas_categorias',array('listas_id'=>$lista->id));

                    $this->db->order_by('posicion','ASC');
                    $lista->detalles = $this->db->get_where('listas_detalles',array('listas_id'=>$lista->id));
                    $foto = $this->querys->get_foto($lista->detalles->row()->adjunto);
                    $this->loadView(array(
                        'view'=>'views/reordenar',
                        'lista'=>$lista,
                        'title'=>$lista->titulo_corto,
                        'foto'=>$foto,
                        'description'=>'The Snackilst: Listas creadas y votadas por todos.'                        
                    ));
                }
            }
        }

        function upload_file(){
            $filename = $_FILES['files']['name'][0];            
            $safefilename = trim(basename(stripslashes($filename)), ".\x00..\x20");
            $tmpfilename = $_FILES['files']['tmp_name'][0];
            $image_info = getimagesize($tmpfilename);

            $image_width = $image_info[0];
            $image_height = $image_info[1];
            if($image_width >= 600 && $image_height >= 600){
            $tempSafeFile = tempnam(sys_get_temp_dir(), "JLC"); //Create a Temp File to store content in a static env.
            switch (exif_imagetype($_FILES['files']['tmp_name'][0])) {
            case IMAGETYPE_JPEG:
                $safefilecontent = imagejpeg(imagecreatefromjpeg($tmpfilename), $tempSafeFile); //Get only file content created a new image and storeing it on my tempfile
                $extensions = array('jpg', 'jpeg');
                $mime = "image/jpeg";
                break;
            case IMAGETYPE_PNG:
                $safefilecontent = imagepng(imagecreatefrompng($tmpfilename), $tempSafeFile);
                $extensions = array('png');
                $mime = "image/png";
                break;
            case IMAGETYPE_GIF:
                $safefilecontent = imagegif(imagecreatefromgif($tmpfilename), $tempSafeFile);
                $extensions = array('gif');
                $mime = "image/gif";
                break;
            case IMAGETYPE_BMP:
                $safefilecontent = image2wbmp(imagecreatefromwbmp($tmpfilename), $tempSafeFile);
                $extensions = array('bmp');
                $mime = "image/x-MS-bmp";
                break;
            //There is a lot of other image types... I use this 4 just for a example
            default :
                throw new Exception("May its a unsafe image file!",500,null);
                break;
            }
            // Adjust incorrect image file extensions:
            if (!empty($extensions)) {
                $parts = explode('.', $safefilename);
                $extIndex = count($parts) - 1;
                $ext = strtolower(@$parts[$extIndex]);
                if (!in_array($ext, $extensions)) {
                    $parts[$extIndex] = $extensions[0];
                    $safefilename = implode('.', $parts);                                        
                }    
            }
            $safefilename = str_replace('á','a',$safefilename);
            $safefilename = str_replace('é','e',$safefilename);
            $safefilename = str_replace('í','i',$safefilename);
            $safefilename = str_replace('ó','o',$safefilename);
            $safefilename = str_replace('ú','u',$safefilename);
            $safefilename = str_replace('à','a',$safefilename);
            $safefilename = str_replace('è','e',$safefilename);
            $safefilename = str_replace('ì','i',$safefilename);
            $safefilename = str_replace('ò','o',$safefilename);
            $safefilename = str_replace('ù','u',$safefilename);
            $safefilename = str_replace('ñ','n',$safefilename);
            $safefilename = mb_strtolower($safefilename);
            $safefilename = str_replace(' ','-',$safefilename);
            $safefilename = preg_replace('/[^.a-z0-9\-]/', '', $safefilename);
            file_put_contents('img/listas/'.$safefilename,file_get_contents($tempSafeFile));
            echo $safefilename;
            }else{
                echo "error";
            }
        }

        function eraseFile($name){
            if(file_exists('img/listas/'.$name)){
                unlink('img/listas/'.$name);
            }
        }

        public function editar($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id) && !empty($_SESSION['user'])){
                $lista = $this->db->get_where('listas',array('id'=>$id,'user_id'=>$this->user->id));
                if($lista->num_rows()>0){                    
                    $lista = $lista->row();
                    //Sumar 1 a las vistas
                    if(empty($_SESSION['user']) || $_SESSION['user']!=$lista->user_id){
                        $this->db->update('listas',array('vistas'=>($lista->vistas+1)),array('id'=>$id));
                    }
                    $this->db->where('original',0);
                    $lista->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$lista->id));
                    $this->db->order_by('posicion','ASC');
                    $lista->detalles = $this->db->get_where('listas_detalles',array('listas_id'=>$lista->id));
                    $this->loadView(array(
                        'view'=>'views/editar-lista',
                        'lista'=>$lista,
                        'title'=>$lista->titulo_corto
                    ));
                }else{
                    throw new Exception('Usted no está autorizado para realizar esta acción',403);
                }
            }else{
                redirect('main');
            }
        }


        public function editarLista(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('listas')
                 ->set_subject('bootstrap2')
                 ->set_theme('bootstrap2')
                 ->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->set_rules('userordenado','USER','callback_validar_filas');

            $crud->fields('userordenado','veces_ordenado');
            $crud->callback_before_update(function($post,$primary){
                $lista = $this->db->get_where('listas',array('id'=>$primary))->row();
                $post['userordenado'] = empty($_SESSION['user'])?'':$_SESSION['user'];
                $post['veces_ordenado'] = $lista->veces_ordenado+=1;
                //Verificar si hay que eliminar imagen anterior                
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){ 
                $lista = $this->db->get_where('listas',array('id'=>$primary))->row();               
                $this->db->delete('listas_categorias',array('listas_id'=>$primary,'original'=>0));
                $this->db->delete('listas_detalles',array('listas_id'=>$primary));

                if(!empty($_POST['detalles']) && count($_POST['detalles'])>0){                  
                    $x = 0;
                    $puntos = 0;
                    $incremento = 10;
                    foreach($_POST['detalles'] as $n=>$v){
                        $data = $v;
                        $data['posicion'] = $x+1;
                        $data['votos'] = count($_POST['detalles'])-$data['posicion'];
                        $data['listas_id'] = $primary;
                        $x++;


                        $detalle = 0;
                        if(!empty($v['id'])){
                            $detalle = $this->db->get_where('listas_detalles',array('id'=>$v['id']));
                            $detalle = $detalle->num_rows()?$detalle->row():0;
                        }


                        if(!empty($v['nombre'])){
                            if(empty($detalle)){
                                $this->db->insert('listas_detalles',$data);
                                $puntos+=$incremento;
                                $incremento-= $incremento>0?1:0;                             
                            }else{
                                $data->posicion = $detalle->posicion;
                                $data->votos = $detalle->votos;
                                $this->db->update('listas_detalles',$data,array('id'=>$detalle->id));
                            }

                        }
                    }
                    //Sumar puntos
                    $puntos += $this->db->get_where('listas',array('id'=>$primary))->row()->votos;
                    $this->db->update('listas',array('votos'=>$puntos),array('id'=>$primary));

                    //Ha reordenado
                    if(!empty($_SESSION['user'])){
                        $hasreordenado = $this->db->get_where('user',array('id'=>$this->user->id))->row()->has_reordenado+1;
                        $this->db->update('user',array('has_reordenado'=>$hasreordenado),array('id'=>$this->user->id));
                        $this->querys->dar_snacks(7,array('user_id'=>$this->user->id,'lista'=>$lista->titulo_corto));
                        $votador = $this->user->nombre;
                    }else{
                        $votador = 'Invitado ';
                    }

                    if(!empty($_POST['categorias_id'])){
                        foreach($_POST['categorias_id'] as $n=>$c){
                            $data = array(
                                'listas_id'=>$primary,
                                'categorias_id'=>$_POST['categorias_id'][$n],
                                'subcategorias_id'=>@$_POST['sub_categorias_id'][$n],
                                'original'=>0
                            );
                            $this->db->insert('listas_categorias',$data);                            
                        }
                    }

                    $this->querys->dar_snacks(8,array('user_id'=>$lista->user_id,'votador'=>$votador,'lista'=>$lista->titulo_corto));
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        

        function share($item,$red = 1){
            if(is_numeric($item)){
                $item = $this->db->get_where('listas',array('id'=>$item));
                if($item->num_rows()>0 && $this->db->get_where('shares',array('listas_id'=>$item->row()->id,'user_id'=>$this->user->id,'red'=>$red))->num_rows()==0){                    
                    $votos = $item->row();
                    if(!empty($_SESSION['user'])){
                        $votador = $this->db->get_where('user',array('id'=>$this->user->id))->row();
                        $hascompartido = $votador->has_compartido+1;
                        $this->db->update('user',array('has_compartido'=>$hascompartido),array('id'=>$this->user->id));                                                
                        $this->db->insert('shares',array('listas_id'=>$votos->id,'user_id'=>$votador->id,'red'=>$red));
                        $votador = $votador->nickname.' ';

                        $this->querys->dar_snacks(2,array('url'=>'lista/'.toUrl($item->row()->id.'-'.$item->row()->titulo_corto),'user_id'=>$this->user->id,'lista'=>$votos->titulo_corto));
                    }else{
                        $votador = 'Usuario invitado ';
                    }                                           
                    $votos->veces_compartido+=1;
                    $this->db->update('listas',array('veces_compartido'=>$votos->veces_compartido),array('id'=>$votos->id));

                    $this->querys->dar_snacks(3,array('url'=>'lista/'.toUrl($item->row()->id.'-'.$item->row()->titulo_corto),'user_id'=>$votos->user_id,'votador'=>$votador,'lista'=>$votos->titulo_corto),'',$this->user->id);
                }
            }
        }

        function plus($item){
            if(is_numeric($item) && !empty($_SESSION['user'])){
                $item = $this->db->get_where('listas_detalles',array('id'=>$item));
                if($item->num_rows()>0){                    
                    $item = $item->row();
                    $votos = $this->db->get_where('listas',array('id'=>$item->listas_id));
                    if($votos->num_rows()>0){ 
                        $lista = $votos->row();
                        //Sumar votos al usuario
                        if(!empty($_SESSION['user'])){
                            $votador = $this->db->get_where('user',array('id'=>$this->user->id))->row();
                            $hasvotado = $votador->has_votado+1;
                            $this->db->update('user',array('has_votado'=>$hasvotado),array('id'=>$this->user->id));                            
                            $votador = $votador->nickname;
                            $this->querys->dar_snacks(4,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$this->user->id,'lista'=>$lista->titulo_corto));
                            $this->db->insert('votos',array('user_id'=>$this->user->id,'listas_detalles_id'=>$item->id,'voto'=>1,'fecha'=>date("Y-m-d H:i:s")));
                            $this->db->insert('listas_log',array('descripcion'=>'1','user_id'=>$this->user->id,'listas_detalles_id'=>$item->id,'voto'=>1,'fecha'=>date("Y-m-d H:i:s")));

                        }
                        else{
                            $votador = 'Usuario invitado ';
                        }
                        $votos = $votos->row();
                        $votos->votos+=1;
                        $this->db->update('listas',array('votos'=>$votos->votos),array('id'=>$votos->id));
                        //Reordenamos la lista
                        $item->votos+=1;
                        $item->votos_positivos+=1;
                        $item->votos_terceros+=1;
                        $this->db->update('listas_detalles',array('votos'=>$item->votos,'votos_terceros'=>$item->votos_terceros,'votos_positivos'=>$item->votos_positivos),array('id'=>$item->id));
                        $this->reordenarlista($votos->id);
                        $this->querys->dar_snacks(5,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$lista->user_id,'votador'=>$votador,'lista'=>$lista->titulo_corto),'',$this->user->id);
                    }
                }
            }
        }

        function minus($item){
            if(is_numeric($item) && !empty($_SESSION['user'])){
                $item = $this->db->get_where('listas_detalles',array('id'=>$item));
                if($item->num_rows()>0){
                    $item = $item->row();
                    $votos = $this->db->get_where('listas',array('id'=>$item->listas_id));
                    if($votos->num_rows()>0){
                        $lista = $votos->row();
                        //Sumar votos al usuario
                        if(!empty($_SESSION['user'])){
                            $votador = $this->db->get_where('user',array('id'=>$this->user->id))->row();
                            $hasvotado = $votador->has_votado+1;
                            $this->db->update('user',array('has_votado'=>$hasvotado),array('id'=>$this->user->id));                            
                            $votador = $votador->nickname;
                            $this->querys->dar_snacks(4,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$this->user->id,'lista'=>$lista->titulo_corto));
                            $this->db->insert('votos',array('user_id'=>$this->user->id,'listas_detalles_id'=>$item->id,'voto'=>-1,'fecha'=>date("Y-m-d H:i:s")));
                            $this->db->insert('listas_log',array('descripcion'=>'1','user_id'=>$this->user->id,'listas_detalles_id'=>$item->id,'voto'=>-1,'fecha'=>date("Y-m-d H:i:s")));

                        }
                        else{
                            $votador = 'Usuario invitado ';
                        }
                        $votos = $votos->row();
                        $votos->votos+=1;
                        $this->db->update('listas',array('votos'=>$votos->votos),array('id'=>$votos->id));
                        //Reordenamos la lista
                        $item->votos-=1;
                        $item->votos_negativos+=1;
                        $item->votos_terceros+=1;
                        $this->db->update('listas_detalles',array('votos'=>$item->votos,'votos_terceros'=>$item->votos_terceros,'votos_negativos'=>$item->votos_negativos),array('id'=>$item->id));
                        $this->reordenarlista($votos->id);   
                        $this->querys->dar_snacks(5,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$lista->user_id,'votador'=>$votador,'lista'=>$lista->titulo_corto),'',$this->user->id);                     
                    }
                }
            }
        }



        function analisis($id = ''){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                if(empty($this->user->id)){
                    redirect('main');
                }
                $this->db->select('listas.*, user.nickname as username, user.foto as perfil');
                $this->db->join('user','user.id = listas.user_id');
                $lista = $this->db->get_where('listas',array('listas.id'=>$id));
                if($lista->num_rows()>0){    
                    $original = $lista->row();                
                    $lista = $lista->row();                    
                    $this->db->where('original',0);
                    $this->db->select('categorias.*');                    
                    $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                    $lista->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$lista->id));
                    
                    $this->db->order_by('posicion','ASC');
                    $lista->detall = $this->db->get_where('listas_detalles',array('listas_id'=>$lista->id));

                    //original                                    
                    $this->db->where('original',1);
                    $this->db->select('categorias.*');                    
                    $this->db->join('categorias','categorias.id = listas_categorias.categorias_id');
                    $original->categorias = $this->db->get_where('listas_categorias',array('listas_id'=>$original->id));
                    
                    $this->db->order_by('posicion','ASC');
                    $original->detalles = $this->db->get_where('listas_detalles_original',array('listas_id'=>$original->id));

                    $this->loadView(array(
                        'view'=>'views/listasvslistas',
                        'lista'=>$lista,
                        'original'=>$original,
                        'title'=>'Analisis de '.$lista->titulo_corto
                    ));
                }
            }else{
                $this->loadView(array(
                    'view'=>'views/listasvslistas',                    
                    'title'=>'Mis Snacks'
                ));
            }
        }

        function snacktrend(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('view_ranking')
                 ->set_subject('bootstrap2')
                 ->set_theme('trends')
                 ->set_primary_key('id')
                 ->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_edit()
                 ->group_by('view_ranking.id')
                 ->order_by('votos','DESC')
                 ->columns('id','fecha','username','titulo','catnom','subcatnom','votos','veces_ordenado')
                 ->set_relation('user_id','user','{nickname}|{foto}')                 
                 ->callback_column('username',function($val,$row){
                    list($nick,$perfil) = explode('|',$row->{'s'.substr(md5('user_id'),0,8)});
                    return '<div class="img-snactrend-fondo" style="background:url('.$this->querys->get_perfil($perfil).'); background-size:cover; width:40px; height:40px;"><img src="'.$this->querys->get_perfil($perfil).'" style="display:none"> </div>'.$nick;
                 })
                 ->callback_column('titulo',function($val,$row){
                    return '<a href="'.base_url('lista/'.toUrl($row->id.'-'.$val)).'">'.$val.'</a>';
                 })
                 ->callback_column('catnom',function($val,$row){
                    return '<a href="'.base_url('categoria/'.toUrl($row->categorias_id.'-'.$val)).'">'.$val.'</a>';
                 })
                 ->callback_column('subcatnom',function($val,$row){
                    return '<a href="'.base_url('categoria/'.toUrl($row->subcategorias_id.'-'.$val)).'">'.$val.'</a>';
                 })
                 ->display_as('user_id','Autor')
                 ->display_as('veces_ordenado','Reordenada')
                 ->display_as('catnom','Categoria')
                 ->display_as('subcatnom','Subcategorias');
            $crud->set_url('listas/frontend/snacktrend/');
            $crud = $crud->render('','theme/theme/views/listados/');            
            $this->loadView(array(
                'view'=>'views/ranking',
                'title'=>'SnackTrends',
                'output'=>$crud->output,
                'js_files'=>$crud->js_files,
                'css_files'=>$crud->css_files
            ));
        }







        public function reordenarListaUserCrud($x = '',$y = ''){
            if(($x=='update_validation' || $x=='update') && is_numeric($y)){
                $this->primary = $y;
            }
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud($this);
            $crud->set_table('listas')
                 ->set_subject('bootstrap2')
                 ->set_theme('bootstrap2')
                 ->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->set_rules('userordenado','USER','required|callback_validar_filas|callback_validar_si_ya_reordeno');

            $crud->fields('userordenado','veces_ordenado');
            
            $crud->callback_before_update(function($post,$primary){
                $lista = $this->db->get_where('listas',array('id'=>$primary))->row();
                $post['userordenado'] = empty($_SESSION['user'])?'':$_SESSION['user'];
                $post['veces_ordenado'] = $lista->veces_ordenado+=1;
                //Verificar si hay que eliminar imagen anterior                
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){                 
                $lista = $this->db->get_where('listas',array('id'=>$primary))->row();

                if(!empty($_POST['detalles']) && count($_POST['detalles'])>0){
                    $x = 0;
                    $puntos = 0;
                    $incremento = 10;
                    foreach($_POST['detalles'] as $n=>$v){

                        $detalle = 0;
                        if(!empty($v['id'])){
                            $detalle = $this->db->get_where('listas_detalles',array('id'=>$v['id']));
                            $detalle = $detalle->num_rows()?$detalle->row():0;
                        }
                        $data = $v;
                        $data['posicion'] = $x+1;
                        $data['votos'] = count($_POST['detalles'])-$data['posicion'];
                        $data['listas_id'] = $primary;
                        $x++;
                        if(!empty($v['nombre'])){
                            if(empty($detalle)){
                                $this->db->insert('listas_detalles',$data);   
                                $det = $this->db->insert_id();                             
                                $this->db->insert('listas_log',array('descripcion'=>'2','user_id'=>$this->user->id,'listas_detalles_id'=>$det,'voto'=>$incremento,'fecha'=>date("Y-m-d H:i:s")));
                            }else{
                                $data['votos']=$detalle->votos+$incremento;
                                $this->db->update('listas_detalles',array('votos'=>$data['votos']),array('id'=>$detalle->id));
                                $this->db->insert('listas_log',array('descripcion'=>'2','user_id'=>$this->user->id,'listas_detalles_id'=>$detalle->id,'voto'=>$incremento,'fecha'=>date("Y-m-d H:i:s")));
                            }

                            $puntos+=$incremento;
                            $incremento-= $incremento>0?1:0;
                        }
                    }

                    //Sumar puntos
                    $lista = $this->db->get_where('listas',array('id'=>$primary))->row();
                    $puntos += $lista->votos;
                    $this->db->update('listas',array('votos'=>$puntos),array('id'=>$primary));

                    //Ha reordenado
                    $hasreordenado = $this->db->get_where('user',array('id'=>$this->user->id))->row()->has_reordenado+1;
                    $this->db->update('user',array('has_reordenado'=>$hasreordenado),array('id'=>$this->user->id));
                    $this->querys->dar_snacks(7,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$this->user->id,'lista'=>$lista->titulo_corto));
                    $votador = $this->user->nombre;       

                    //Ordenar por posicion
                    $this->reordenarlista($primary);          

                    $this->querys->dar_snacks(8,array('url'=>'lista/'.toUrl($lista->id.'-'.$lista->titulo_corto),'user_id'=>$lista->user_id,'votador'=>$votador,'lista'=>$lista->titulo_corto),'',$this->user->id);
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function validar_filas(){
            if(!empty($_POST) && !empty($_POST['detalles'])){
                $x = 0;
                foreach($_POST['detalles'] as $d){
                    if(!empty($d['nombre']) && !empty($d['autor'])){
                        $x++;
                    }
                }
                if($x>=3){
                    //Validamos que todos los enviados tengan nombre y autor
                    foreach($_POST['detalles'] as $d){
                        if(!empty($d['adjunto']) && (empty($d['autor']) || empty($d['nombre']))){
                            $this->form_validation->set_message('validar_filas','No está permitido enviar una imagen sin nombre ni autor.');
                            return false;
                        }
                    }

                    return true;
                }
            }
            $this->form_validation->set_message('validar_filas','Una lista debe contener al menos 3 elementos');
            return false;
        }



        function validar_si_ya_reordeno($primary){

            $this->db->join('listas_detalles','listas_detalles.id = listas_log.listas_detalles_id');
            $this->db->join('listas','listas.id = listas_detalles.listas_id');
            if(empty($this->primary) || $this->db->get_where('listas_log',array('descripcion'=>'2','listas_log.user_id'=>$this->user->id,'listas.id'=>$this->primary))->num_rows()>0){
                $this->form_validation->set_message('validar_si_ya_reordeno','Ya has reordenado esta lista anteriormente. Solo está permitido reordenar una sola vez por cada lista.');
                return false;
            }
            return true;
        }

        protected function reordenarlista($id){            
            $this->querys->reordenarlista($id);
        }


    }
?>
