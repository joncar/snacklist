<nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate">
            <a href="index.php" class="logo-header"><img src="assets/img/logo-snacklist.png" alt="Logo Snacklist"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse js-navbar-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="nav-item">
                    <div class="search__container"><input class="search__input" type="text" placeholder="Buscar"></div>
                </li>
            </ul>

            <ul class="nav navbar-nav">
              <li class="dropdown mega-dropdown">
                <a href="#" class="dropdown-toggle btn-menu-top-sesion" data-toggle="dropdown">Categorias</a>

                <ul class="dropdown-menu mega-dropdown-menu row" style="margin-left:0px; margin-right:0px;">
                  <li class="col-sm-4">
                    <ul>
                      <li class="dropdown-header">Destacadas</li>
                      <li class="divider"></li>
                      <li><a href="#">Categoría 1</a></li>
                      <li><a href="#">Categoría 1</a></li>
                      <li><a href="#">Categoría 1</a></li>
                      <li><a href="#">Categoría 1</a></li>
                      <li><a href="#">Categoría 1</a></li>
                    </ul>
                  </li>

                  <div class="orden-alfabetico-menu">
                      <li class="col-sm-3">
                        <ul>
                          <li class="dropdown-header"><b>A</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                          <li class="dropdown-header"><b>E</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                        </ul>
                      </li>
                      <li class="col-sm-3">
                        <ul>
                          <li class="dropdown-header"><b>B</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">RCategoría</a></li>
                          <li class="divider"></li>
                          <li class="dropdown-header"><b>F</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                        </ul>
                      </li>
                      <li class="col-sm-3">
                        <ul>
                          <li class="dropdown-header"><b>C</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                          <li class="dropdown-header"><b>G</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                        </ul>
                      </li>
                      <li class="col-sm-3">
                        <ul>
                          <li class="dropdown-header"><b>D</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                          <li class="dropdown-header"><b>H</b></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li><a href="#">Categoría</a></li>
                          <li class="divider"></li>
                        </ul>
                      </li>
                    </div>
                </ul>

              </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-facebook-official"></i></a></li>
                <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-twitter"></i></a></li>
                <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-google-plus-official"></i></a></li>
                <li class="nav-item"><a href="publicar-lista.php" class="btn-menu-top"><b>Publica tu lista</b></a></li>

                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/Iconos/notificaciones.png" class="img-notificaciones"> <span class="badge badge-light">4</span></a>
                    <div class="dropdown-menu" id="dropdown-menu03">
                        <div class="row fondo-iniciar-sesion02 text-center">
                            <div class="col-sm-12 notificacion-general">
                                <div class="snacks-notificaciones01">+50 snacks.</div>
                                ¡Bienvenido a The Snacklist! En este espacio tendrás tanta voz como tú desees ¡aprovéchala!
                            </div><hr>
                            <div class="col-sm-12 notificacion-gamification">
                                <div class="snacks-notificaciones02">+20 snacks.</div>
                                Publicaste la lista "Nombre de la lista"
                            </div><hr>
                            <div class="col-sm-12 notificacion-snacktrend">
                                <div class="snacks-notificaciones03">+ 100 snacks.</div>
                                ¡Felicidades! Tu lista "(Nombre de la lista)" ha subido de posición en the Snacktrend a #(Nueva posición)
                            </div><hr>
                            <a href="notificaciones.php"><div class="col-sm-12 btn-ver-notificaciones">Ver todas las notificaciones</div></a>
                        </div>
                    </div>
                </li>

                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/Iconos/sesion.png" class="img-notificaciones"></a>
                    <div class="dropdown-menu" id="dropdown-menu02">
                        <div class="row fondo-iniciar-sesion02 text-center">
                            <div class="col-sm-12 texto-aviso-login02">Mis listas</div>
                            <div class="col-sm-12 texto-aviso-login02">Editar mi Perfil</div>
                            <div class="col-sm-12 texto-aviso-login02"><i class="material-icons">call_missed_outgoing</i>Cerrar sesión</div>
                        </div>
                    </div>
                </li>
            </ul>

        </div>
    </div>
</nav>

<script>
jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
})
</script>
