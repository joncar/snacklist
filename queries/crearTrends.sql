#PROCEDURE CALL crearTrends();
BEGIN
SET @pos = 0;
SELECT
@pos:=(@pos+1) AS posicion,
t.* FROM (SELECT 
listas.id as listas_id,
categorias.id as categorias_id,
categorias.nombre as catnom,
sub_categorias.id as subcategorias_id,
sub_categorias.nombre as subcatnom,
IFNULL(listas_trends.fecha,DATE(NOW())) as fecha_trend,
listas.votos
FROM listas
RIGHT JOIN listas_categorias ON listas_categorias.listas_id = listas.id
INNER JOIN categorias ON categorias.id = listas_categorias.categorias_id
LEFT JOIN sub_categorias ON sub_categorias.id = listas_categorias.subcategorias_id
LEFT JOIN (SELECT *,MAX(posicion) as pos FROM listas_trends GROUP BY listas_trends.listas_id) AS listas_trends ON listas_trends.listas_id = listas.id
          GROUP BY listas.id
) AS t ORDER BY t.votos DESC,t.fecha_trend ASC LIMIT 10;
END