<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <div class="container">
            <?php include('menu.php');?>
        </div>
    </div>

    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>


            <div class="row margin-categorias-tarjetas">
                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="subcategorias.php">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>

                <div class="col-sm-3">
                      <div class="table-responsive tabla-categorias">
                          <table class="table table-striped">
                              <thead class="titulo-categorias">
                                  <div class="media header-categoria">
                                      <div class="imagen-categorias center-block"><img src="assets/img/Iconos/icono-gastronomia.png"></div>
                                      <div class="media-body"><b>Gastronomía y<br>Restaurantes</b></div>
                                  </div>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                                  <tr>
                                      <td><a href="#">127</a></td>
                                      <td><a href="#">Animales y Mascotas</a></td>
                                  </tr>
                              </tbody>
                          </table>
                     </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>
</body>
</html>
