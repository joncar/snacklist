<div id="slider-gris-01" class="contenedor-recomendaciones">
    <div class="row titulo-redeem texto-negro"><h2>Recomendaciones</h2></div>
    <div class="row slider-gris-01">
      <!-- Tarjeta -->
      <div class="col-sm-3">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="col-sm-3">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="col-sm-3">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
      <!-- Tarjeta -->
      <div class="col-sm-3">
          <div class="contenedor-tarjeta-blanco fondo-blanco">
                <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                <div class="header-tarjeta-categoria texto-gris-oscuro">
                    <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                </div>
                <a href="#">
                    <div class="header-tarjeta-categoria texto-gris-oscuro">
                        <h3>Mejores canciones rockeras de todos los tiempos</h3>
                    </div>
                    <div class="footer-tarjeta-snacktrend-top6">
                        <div class="contenedor-listado-categoria">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                        <div class="contenedor-listado-categoria ">
                            <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                            <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                        </div>
                    </div>
                </a>
                <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                    <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                    <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                </div>
          </div>
      </div>
    </div>
</div>
