DROP VIEW IF EXISTS view_snacks;
CREATE VIEW view_snacks AS 
SELECT 
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 1) as registro,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 6) as listas,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 8) as ediciones_terceros,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 3) as te_han_compartido,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 5) as votos_terceros,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 7) as has_reordenado,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 2) as has_compartido,
(SELECT IFNULL(SUM(snacks),0) AS total FROM snacks as sn where sn.user_id = snacks.user_id AND sn.motivo = 4) as has_votado,
SUM(snacks) as total,
snacks.user_id
FROM snacks
GROUP BY snacks.user_id