<div>[menu]</div>

    <!-- Header / End -->

    <!-- Content -->
    <div id="content" class="bg-light">

        <!-- Post / Single -->
        <article class="post single">
            <div class="post-image bg-parallax">
                <img src="[foto]" alt="">
            </div>
            <div class="container container-md">
                <div class="post-content">
                    <ul class="post-meta">
                        <li>[fecha]</li>
                        <li>per [user]</li>
                    </ul>
                    <h1 class="post-title">
                        [titulo]
                    </h1>
                    <hr>
                    [texto]
                    
                </div>
            </div>
        </article>

        <div>[footer]</div>

    </div>
    <!-- Content / End -->