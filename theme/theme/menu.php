
    <nav class="navbar navbar-color-on-scroll fixed-top  navbar-expand-lg " color-on-scroll="100" id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <a href="index.php" class="logo-header"><img src="assets/img/logo-snacklist.png" alt="Logo Snacklist"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>


            <div class="collapse navbar-collapse js-navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li class="nav-item">
                        <div class="search__container"><input class="search__input" type="text" placeholder="Buscar"></div>
                    </li>
                </ul>

                <ul class="nav navbar-nav">
                  <li class="dropdown mega-dropdown">
                    <a href="#" class="dropdown-toggle btn-menu-top-sesion" data-toggle="dropdown">Categorias</a>

                    <ul class="dropdown-menu mega-dropdown-menu row" style="margin-left:0px; margin-right:0px;">
                      <li class="col-sm-4">
                        <ul>
                          <li class="dropdown-header">Destacadas</li>
                          <li class="divider"></li>
                          <li><a href="#">Categoría 1</a></li>
                          <li><a href="#">Categoría 1</a></li>
                          <li><a href="#">Categoría 1</a></li>
                          <li><a href="#">Categoría 1</a></li>
                          <li><a href="#">Categoría 1</a></li>
                        </ul>
                      </li>

                      <div class="orden-alfabetico-menu">
                          <li class="col-sm-3">
                            <ul>
                              <li class="dropdown-header"><b>A</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><b>E</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                            </ul>
                          </li>
                          <li class="col-sm-3">
                            <ul>
                              <li class="dropdown-header"><b>B</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">RCategoría</a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><b>F</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                            </ul>
                          </li>
                          <li class="col-sm-3">
                            <ul>
                              <li class="dropdown-header"><b>C</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><b>G</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                            </ul>
                          </li>
                          <li class="col-sm-3">
                            <ul>
                              <li class="dropdown-header"><b>D</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                              <li class="dropdown-header"><b>H</b></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li><a href="#">Categoría</a></li>
                              <li class="divider"></li>
                            </ul>
                          </li>
                        </div>
                    </ul>

                  </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-facebook-official"></i></a></li>
                    <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-twitter"></i></a></li>
                    <li class="nav-item"><a href="#" class="nav-link" id="icon-menu-top"><i class="fa fa-google-plus-official"></i></a></li>

                    <li class="nav-item"><a href="publicar-lista.php" class="btn-menu-top"><b>Publica tu lista</b></a></li>

                    <li class="dropdown nav-item">
                        <a href="#" class="dropdown-toggle btn-menu-top-sesion" data-toggle="dropdown"><b>Iniciar sesión</b></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="row fondo-iniciar-sesion text-center">
                                <div class="col-sm-12"><b>Iniciar sesión</b></div>
                                <!-- <div class="center-block" id="img-login-registro"><i class="material-icons">person_pin</i></div> -->
                                <div class="col-sm-12">
                                    <input class="form-control margin-login-input" id="prueba" placeholder="Correo" type="email">
                                    <input class="form-control margin-login-input" id="prueba" placeholder="Contraseña" type="password" maxlength="8">
                                </div>
                                <div class="col-sm-12 recuperar-cuenta" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#recuperar-contraseña">
                                    <a href="#"><b>Recuperar contraseña por correo</b></a>
                                </div>
                                <div class="col-sm-12">
                                    <div class="login-btn">Iniciar sesión</div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="login-fb">
                                        <i class="fa fa-facebook-official"></i> Login con Facebook
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="login-cuenta" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#login">
                                        <a href="#"><b>Registrarme</b></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

<script>
jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
})
</script>
