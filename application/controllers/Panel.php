<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url('registro/index/add'));
                    die();
                }                
                if(!$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                    exit;
                }
        }
        
        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posseeix permisos per a realitzar aquesta operació','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = $this->user->admin==1?'templateadmin':'template';
                    $this->load->view($template,$param);
                }                
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
              //  $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));
            if(!empty($this->norequireds)){
                $crud->norequireds = $this->norequireds;
            }
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }        /*Cruds*/              
        
        public function index() {
            if($this->user->admin==0){
                redirect('mis-listas');
            }
            $this->loadView('panel');
        }                
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
