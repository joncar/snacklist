<!-- Item 01 -->
<?php
    $this->db->order_by('orden','ASC');
    $this->db->select('listas.*, sliders.foto');
    $this->db->join('listas','listas.id = sliders.listas_id');
?>
<?php foreach($this->db->get_where('sliders')->result() as $l): ?>
    <div class="item" style="background:url(<?= base_url('img/sliders/'.$l->foto) ?>)">
    
        <img src="<?= base_url('img/sliders/'.$l->foto) ?>" alt="" style="position:absolute; width:100%;">
        <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
            <div class="row">
                <div class="col-md-12 padding0 text-center"><h1><?= $l->titulo_corto ?></h1></div>
            </div>
        </div>
        <div class="container-fluid lista-slider-top texto-blanco">
            <div class="row">
                <div class="col-sm-12">
                    <?php
                        //Conseguir 3 numeros aleatorios
                        $lista = $this->db->get_where('listas_detalles',array('listas_id'=>$l->id));
                        $num = $lista->num_rows()-1;
                        $num = $num==0?$lista->num_rows():$num;
                        $n1 = rand(0,$num);
                        $n2 = $n1;
                        do{$n2 = rand(0,$num); $n3 = $n2;}while($n2==$n1);
                        do{$n3 = rand(0,$num);}while($n3==$n2);

                        foreach($lista->result() as $n=>$ll): if($n==$n1 || $n==$n2 || $n==$n3): ?>
                        <div class="col-sm-3 col-md-3">
                            <div class="listado-slider-bottom">
                                <a href="<?= base_url('lista/'.toUrl($l->id.'-'.$l->titulo_corto)) ?>"><?= $ll->posicion ?>. <?= $ll->nombre ?></a>
                            </div>
                        </div>
                    <?php endif; endforeach ?>

                    <div class="col-sm-3 col-md-3">
                        <div class="listado-slider-bottom btn-listado-slider-top btn-general center-block" style="width: 50%;">
                            <a href="<?= base_url('lista/'.toUrl($l->id.'-'.$l->titulo_corto)) ?>">Ver Lista</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
