<!DOCTYPE html>
<html lang="en">
<head>
<!-- Librerias -->
<?php include('head.php');?>
</head>
<body>
    <!-- Menu -->
    <div class="container-fluid fondo-negro texto-blanco">
        <?php include('menu.php');?>
    </div>

    <div class="container-fluid container-fluid fondo-gradient-gris-oscuro contenedor-listado-tarjetas">
        <div class="container">

          <div class="row margin-subcategorias">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

          <div class="row margin-subcategorias">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

          <div class="row">
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
            <!-- Tarjeta -->
            <div class="col-sm-3">
                <div class="contenedor-tarjeta-blanco fondo-blanco">
                      <div class="vistas-tarjetas icon-vistas"><i class="fa fa-eye" aria-hidden="true"></i> 100</div>
                      <div class="header-tarjeta-categoria texto-gris-oscuro">
                          <h3><a href="#"><span class="categoria-snacktrend">/Música y entretenimiento</span></a></h3>
                      </div>
                      <a href="#">
                          <div class="header-tarjeta-categoria texto-gris-oscuro">
                              <h3>Mejores canciones rockeras de todos los tiempos</h3>
                          </div>
                          <div class="footer-tarjeta-snacktrend-top6">
                              <div class="contenedor-listado-categoria">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">2</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">5</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                              <div class="contenedor-listado-categoria ">
                                  <div class="imagen-listado-tarjeta"><div class="top-listado-tarjeta texto-blanco">10</div></div>
                                  <div class="listado-tarjeta"><b>Sweet child of mine</b><br>Guns and Roses</div>
                              </div>
                          </div>
                      </a>
                      <div class="titulo-tarjeta-categoria texto-gris-oscuro">
                          <div class="perfil-tarjeta-categorias"><img src="assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-circle"></div>
                          <div class="username-tarjetas"><b>Username 911</b><br>Votos:718</div>
                      </div>
                </div>
            </div>
          </div>

        </div>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Select Input -->
    <script>
    function DropDown(el) {
			this.dd = el;
			this.placeholder = this.dd.children('span');
			this.opts = this.dd.find('.dropdown a');
			this.val = '';
			this.index = -1;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;
				obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					return false;
				});
				obj.opts.on('click',function(){
					var opt = $(this);
					obj.val = opt.text();
					obj.index = opt.index();
					obj.placeholder.text(obj.val);
				});
			},
			getValue : function() {
				return this.val;
			},
			getIndex : function() {
				return this.index;
			}
		}
		$(function() {
			var dd = new DropDown( $('#dd') );
		});
    $(function() {
			var dd = new DropDown( $('#dd2') );
		});
    </script>

    <!-- Add / Remove -->
    <script>
    /* Variables */
    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {
    row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
    button.closest("div").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($("#participantTable div").length < 17) {
      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(".remove").on('click', function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
</body>
</html>
