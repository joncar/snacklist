<!-- Tarjeta -->
<div class="col-sm-3 margin-bottom-listas-nuevas">
  <div class="contenedor-tarjeta-blanco-categorias fondo-blanco">

					<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px; margin-bottom:  30px;">
          <div class="col-xs-6 col-sm-6" title="Vistas de la Lista" style="padding-right:0px; padding-left:0px;">
              <div class="vistas-tarjetas icon-vistas">
                <i class="fa fa-eye" aria-hidden="true"></i> <?= $l->vistas ?>
              </div>
          </div>

          <div class="col-xs-6 col-sm-6" title="Fecha de creación de la Lista" style="padding-right:0px; padding-left:0px;">
              <div class="fecha-tarjetas icon-vistas"><?= $l->fecha_format ?></div>
          </div>
      </div>


      <a href="<?= $l->link ?>">
          <div class="header-tarjeta-categoria texto-gris-oscuro">
              <h3><?= $l->titulo_corto ?></h3>
          </div>
          <div class="height-listas-principales">
            <?php foreach($l->detalles_sort as $d): ?>
                <div class="col-xs-12 col-sm-12" style="display:flex; margin-bottom:10px;">
                    <div class="imagen-listado-tarjeta" style="background:url('<?= $d->adjunto ?>'); background-size: cover; background-position:center center; width: 60px;height: 60px;">
                      <div class="top-listado-tarjeta texto-blanco" style="width: 60px;height: 60px;"><?= $d->posicion ?></div>
                    </div>
                    <div class="listado-tarjeta"><b><?= $d->nombre ?></b><br><?= $d->autor ?></div>
                </div>
            <?php endforeach ?>
              
          </div>
      </a>



					<div class="col-xs-12 col-sm-12 padding0" style="padding-right:0px; padding-left:0px;">
          <div class="titulo-tarjeta-categoria texto-gris-oscuro">
              <div class="perfil-tarjeta-categorias-slider" style="background:url(<?= $l->perfil; ?>); background-size: cover; background-position: center; border-radius:100%;">
              	<!--<img src="<?= $l->perfil; ?>" alt="Perfil Snacktrend" class="center-block img-responsive img-circle">-->
              </div>
              <div class="username-tarjetas"><b> <?= $l->username ?> </b><br>Votos:<?= $l->votos ?></div>
          </div>
					</div>

  </div>
</div>
              <!-- Tarjeta -->