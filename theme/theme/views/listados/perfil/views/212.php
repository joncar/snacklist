<?php
foreach ($list as
        $num_row =>
        $row) {
    ?>        
    <tr <?php if ($num_row % 2 == 1) { ?>class="erow"<?php } ?>>
    <?php foreach ($columns as
            $column) {
        ?>
            <td class='<?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>sorted<?php } ?>'>
                <div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;'; ?></div>
            </td>
    <?php } ?>
    <?php if (!$unset_delete || !$unset_edit || !$unset_read || !$unset_clone || !empty($actions)) { ?>
            <td align="right">

                <div class="hidden-sm hidden-xs btn-group">
                    <?php if(!$unset_read):?>
                        <a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="btn btn-xs btn-success">
                            <i class="ace-icon fa fa-arrow-right bigger-120"></i>
                        </a>
                    <?php endif ?>
                    <?php if(!$unset_edit):?>
                    <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="btn btn-xs btn-info">
                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                    </a>
                    <?php endif ?>         
                    <?php if(!$unset_clone):?>
                    <a href='<?php echo $row->clone_url?>' title='Clonar <?php echo $subject?>' class="btn btn-xs btn-warning">
                        <i class="ace-icon fa fa-clone bigger-120"></i>
                    </a>
                    <?php endif ?>
                    <?php if(!$unset_delete):?>
                    <a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="btn btn-xs btn-danger delete-row">
                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                    </a>
                    <?php endif ?>
                    <?php if(!empty($row->action_urls) || get_instance()->db->table_exists('acciones')):?>
                    <button class="btn btn-xs btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-position="auto" aria-expanded="false">
                        <i class="ace-icon fa fa-list icon-only bigger-120"></i>
                    </button>
                    <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                    <?php if(!empty($row->action_urls)):?>                    
                    
                        <?php                        
                            foreach($row->action_urls as $action_unique_id => $action_url):
                                            $action = $actions[$action_unique_id];                        
                        ?>
                            <li>
                                <a href="<?php echo $action_url; ?>" class="tooltip-info <?php echo $action->css_class; ?> crud-action"><?php 
                                    ?><?= $action->label ?><?php    
                                ?></a>
                            </li>
                        <?php endforeach ?>
                    
                    <?php endif ?>
                    
                    <!--- Acciones puestas por los usuarios -->
                    <?php 
                        if(get_instance()->db->table_exists('acciones')): 
                            get_instance()->db->order_by('posicion','ASC');
                            foreach(get_instance()->db->get_where('acciones',array('controlador'=>get_instance()->router->fetch_class(),'funcion'=>get_instance()->router->fetch_method()))->result() as $a):
                    ?>
                        <li>
                            <?php 
                                $primary = empty($primary_key_list_actions)?$primary_key:$primary_key_list_actions;
                            ?>
                            <a href="<?php echo base_url($a->ruta).'/'.$row->{$primary}; ?>" class="tooltip-info crud-action"><?php 
                                  ?><?= $a->etiqueta ?><?php    
                              ?></a>
                        </li>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php endif ?>
                    </ul>
                </div>

                <div class="hidden-md hidden-lg">
                    <div class="inline pos-rel">
                        <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto" aria-expanded="false">
                            <i class="ace-icon fa fa-cog icon-only bigger-110"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                            <?php if(!$unset_read){?>
                                <li role="presentation">
                                    <a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button">
                                          <i class="glyphicon glyphicon-search"></i> Ver
                                   </a>
                                </li>
                                <?php }?>
                                <?php if(!$unset_edit){?>
                               <li role="presentation">
                                    <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button">
                                          <i class="glyphicon glyphicon-edit"></i> Editar
                                    </a>
                                </li>
                                <?php }?>     
                                <?php if(!$unset_clone){?>
                                <li role="presentation">
                                    <a href='<?php echo $row->clone_url?>' title='<?php echo $this->l('list_clone')?> <?php echo $subject?>' class="edit_button">
                                          <i class="glyphicon glyphicon-copy"></i> <?php echo $this->l('list_clone') ?>
                                    </a>
                                </li>
                                <?php }?>                          
                                <?php 
                                      if(!empty($row->action_urls)){
                                      foreach($row->action_urls as $action_unique_id => $action_url){ 
                                              $action = $actions[$action_unique_id];
                                  ?>
                                <li role="presentation">
                                    <a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action"><?php 
                                          ?><?= $action->label ?><?php  
                                      ?></a>
                                </li>
                                <?php }
                                  }?>
                                
                                <!--- Acciones puestas por los usuarios -->
                                <?php 
                                    if(get_instance()->db->table_exists('acciones')): 
                                        get_instance()->db->order_by('posicion','ASC');
                                        foreach(get_instance()->db->get_where('acciones',array('controlador'=>get_instance()->router->fetch_class(),'funcion'=>get_instance()->router->fetch_method()))->result() as $a):
                                ?>
                                    <li role="presentation">
                                        <?php 
                                            $primary = empty($primary_key_list_actions)?$primary_key:$primary_key_list_actions;
                                        ?>
                                        <a href="<?php echo base_url($a->ruta).'/'.$row->{$primary}; ?>" class="crud-action"><?php 
                                              ?><?= $a->etiqueta ?><?php    
                                          ?></a>
                                    </li>
                                    <?php endforeach ?>
                                <?php endif ?>
                                <?php if(!$unset_delete){?>
                                <li role="presentation">
                                    <a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row" >
                                          <i class="glyphicon glyphicon-remove"></i> Borrar
                                    </a>
                                </li>
                                <?php }?>
                        </ul>
                    </div>
                </div>
            </td>
    <?php } ?>
    </tr>
<?php } ?>
