<div>
  <?php
      $idiomas = $this->db->get('ajustes')->row()->idiomas;
      $idiomas = explode(',',$idiomas);
  ?>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <?php foreach($idiomas as $n=>$v): ?>
        <li role="presentation" <?= $n==0?'class="active"':'' ?>>
          <a href="#tab<?= $n ?>" aria-controls="home" role="tab" data-toggle="tab"><?= trim($v); ?></a>
        </li>
    <?php endforeach ?>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">    
    <?php foreach($idiomas as $n=>$v): ?>
        <div role="tabpanel" class="tab-pane <?= $n==0?'active':'' ?>" id="tab<?= $n ?>">
          <?php echo $textos[trim($v)]->output ?>          
        </div>
    <?php endforeach ?>
  </div>

</div>