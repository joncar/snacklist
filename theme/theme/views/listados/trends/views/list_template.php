<?php
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');

if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . 'trends/js/cookies.js');
$this->set_js($this->default_theme_path . 'trends/js/flexigrid.js');
$this->set_js($this->default_theme_path . 'trends/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . 'trends/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . 'trends/js/pagination.js');
/** Fancybox */
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>
<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="container fondo-preguntas padding0 flexigrid" data-unique-hash="<?php echo $unique_hash; ?>">
	<div class="row titulo-redeem texto-negro text-center">
    <div class="col-md-12 text-center"><h2><b>Ranking</b></h2></div>
	</div>
	<div class="row padding0">
		<div class="col-sm-12 padding0">

			<div class="col-sm-6">
				<div class="form-group">
					<select id="selectbasic" name="search_text[]" class="form-control select-publicar">
						<option value="" id="input-publicar">Seleccione una categoria</option>
						<?php foreach(get_instance()->db->get('categorias')->result() as $c): ?>
						<option value="<?= $c->id ?>"><?= $c->nombre ?></option>
						<?php endforeach ?>
					</select>
					<input type="hidden" name="search_field[]" value="categorias_id">
				</div>
			</div>

      <div class="col-sm-6">
				<div class="form-group">
					<select id="selectbasic" name="order_by[0]" class="form-control select-publicar">
						<option value="" id="input-publicar">Ordenar por</option>
            <option value="votos">Votos</option>
						<option value="veces_ordenado">Reordenada</option>
						<option value="fecha">Fecha</option>
					</select>
					<input type="hidden" name="order_by[1]" class='hidden-ordering'  value='DESC'/>
				</div>
			</div>

      <!--
			<div class="col-sm-6">
				<div class="form-group">
					<select id="selectbasic" name='order_by[0]' class="form-control order">
						<option value="votos">Votos</option>
						<option value="veces_ordenado">Reordenada</option>
						<option value="fecha">Fecha</option>
					</select>
					<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='DESC'/>
				</div>
			</div>-->

		</div>
	</div>
	<div class="row">
		<div class="table-responsive">

			<?php if (!empty($list)): ?>
		        <div class="ajax_list">
		            <?php echo $list_view?>
		        </div>
		    <?php else: ?>
		        Sin datos para mostrar
		    <?php endif; ?>
		    <div class="row">
		        <div class="col-xs-6">
		            <div class="dataTables_paginate paging_simple_numbers pageContent" id="dynamic-table_paginate">
		                <ul class="pagination">
		                </ul>
		            </div>
		        </div>
		    </div>

		</div>
	</div>
</div>

<input type='hidden' name='per_page' class='hidden-sorting' value='10' />
<?php echo form_close() ?>
