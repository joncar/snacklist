DROP VIEW IF EXISTS view_get_categoria2;
CREATE VIEW view_get_categoria2 AS
SELECT 
    listas_categorias.id,
    listas_categorias.listas_id,
    categorias.nombre as categoria,
    sub_categorias.nombre as subcategoria
    FROM listas_categorias
    LEFT JOIN categorias ON listas_categorias.categorias_id = categorias.id
    LEFT JOIN sub_categorias ON listas_categorias.subcategorias_id = sub_categorias.id  
    WHERE original = 1      
    GROUP BY listas_categorias.id
    ORDER BY listas_categorias.id DESC    