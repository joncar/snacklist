<!-- Item 01 -->
<div class="item background-cover fondo-slider01">
    <div class="container-fluid titulo-slider-top texto-blanco text-shadow">
        <div class="col-md-12 padding0 text-center"><h1>Viajes</h1></div>
    </div>
    <div class="container lista-slider-top texto-blanco">
        <div class="row">
            <div class="col-md-4">
                <div class="listado-slider-bottom"><a href="">Hoteles de Playa<br>(127)</a></div>
            </div>
            <div class="col-md-4">
                <div class="listado-slider-bottom"><a href="">Hoteles románticos<br>(165)</a></div>
            </div>
            <div class="col-md-4">
                <div class="listado-slider-bottom"><a href="">Hoteles de lujo<br>(127)</a></div>
            </div>
        </div>
    </div>
</div>
