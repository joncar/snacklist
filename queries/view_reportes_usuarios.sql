DROP view IF exists view_reportes_usuarios;
CREATE VIEW view_reportes_usuarios AS 
SELECT 
user.fecha_registro AS fecha,
user.id as id,
user.nickname as nombre,
IF(user.status=1,'Activo','Bloqueado') AS status,
user.password as contrasena,
user.fecha_cumpleanos as cumpleanos,
user.genero,
user.cp
FROM user