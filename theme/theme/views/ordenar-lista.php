
    <div class="container-fluid contenedor-ads">
        <div class="container">
            <div class="row text-center">Google ADS</div>
        </div>
    </div>

    <div class="container-fluid contenedor-snackteam fondo-blanco">
        <form action="" onsubmit="return guardarLista(this)" method="post">
            <div class="container">
                <div class="row titulo-redeem texto-negro"><h2>Editar lista</h2></div>

                <div class="row">
                    <div class="col-sm-12">
                        Título de la lista
                        <h3><b><?= $lista->titulo ?></b></h3>
                    </div>
                    <div class="col-sm-12">
                        <?= $lista->titulo_corto ?>
                        <h3><b>Mejores canciones rockeras</b></h3>
                    </div>
                </div>

                <div class="row titulo-redeem texto-negro"><h2>Clasificación</h2></div>

                <div class="row contenedor-select-publicar">
                    <div class="col-sm-12 padding0" id="participantTable">
                        
                        <?php foreach($lista->categorias->result() as $c): ?>
                            <div class="participantRow">
                                <div class="col-sm-6">
                                    <h3>Categoría</h3>                            
                                    <div class="form-group">
                                        <select id="selectbasic" name="categorias_id[]" class="form-control">
                                            <option value="">Seleccione una opción</option>
                                          <?php foreach($this->db->get_where('categorias',array())->result() as $b): ?>
                                                <option value="<?= $b->id ?>" <?= $b->id==$c->categorias_id?'selected':'' ?>><?= $b->nombre ?></option>
                                          <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <h3>Subcategoría</h3>                            
                                    <div class="form-group">
                                        <select id="selectbasic" name="sub_categorias_id[]" class="form-control">
                                            <option value="">Seleccione una opción</option>
                                            <?php foreach($this->db->get_where('sub_categorias',array())->result() as $b): ?>
                                                    <option value="<?= $b->id ?>" <?= $b->id==$c->subcategorias_id?'selected':'' ?>><?= $b->nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <button class="remove btn-remover-categoria" type="button"><b>Eliminar</b></button>
                            </div>
                        <?php endforeach ?>

                    </div>

                    <div class="col-sm-12">
                        <div id="addButtonRow"><button class="add btn-agregar-categoria" type="button"><b>Agregar</b></button></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 texto-input-publicar">Arrastra cualquier elemento para reposicionarlo</div>
                    <div class="col-sm-12 contenedor-tabs-redeem">
                        <div class='parent'>
                          <div class='wrapper'>
                            <div id='left-rm-spill' class='container-drag'>

                                <!-- Tarjeta publicar -->
                                <?php for($i=0;$i<10;$i++): ?>
                                    <!-- Tarjeta publicar -->
                                    <div class="col-sm-12 contenedor-tarjeta-redeem">
                                        <div class="col-sm-1 numero-votar">
                                            <i class="material-icons">arrow_drop_up</i><br>
                                            <?= $i+1 ?><br>
                                            <i class="material-icons">arrow_drop_down</i>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="col-sm-12">
                                                <input class="form-control margin-login-input" id="input-publicar-lista" name="detalles[<?= $i ?>][nombre]" placeholder="Nombre del elemento" type="text" value="<?= !empty($lista->detalles->row($i)->nombre) && $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->nombre:'' ?>">
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-4" style="padding-left:0px; padding-right:0px;">
                                                    <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-foto-lista">
                                                          <i class="material-icons">camera_alt</i>
                                                    </div>

                                                    <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-video-lista">
                                                          <i class="material-icons">play_arrow</i>
                                                    </div>

                                                    <div class="iconos-publicar iconos-publicar-lista" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#seleccionar-gif-lista">
                                                          <i class="material-icons">gif</i>
                                                    </div>
                                                </div>
                                                <div class="col-sm-8" style="padding-left:0px; padding-right:0px;">
                                                    <input class="form-control margin-login-input" id="input-publicar-lista"  name="detalles[<?= $i ?>][autor]" placeholder="Autor de la imagen" type="text" value="<?= !empty($lista->detalles->row($i)->autor) && $i<$lista->detalles->num_rows()?$lista->detalles->row($i)->autor:'' ?>">
                                                    <input type="hidden" name="detalles[<?= $i ?>][adjunto]" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <img src="<?= base_url() ?>theme/theme/assets/img/Perfiles/perfil.jpg" alt="Perfil Snacktrend" class="center-block img-responsive img-redeem">
                                        </div>
                                        <div class="col-sm-1 icon-redeem" type="button" rel="tooltip" data-original-title="" data-toggle="modal" data-target="#eliminar-lista">
                                            <div class="iconos-publicar iconos-publicar-lista"><i class="material-icons">delete_forever</i></div>
                                        </div>
                                    </div>
                                <?php endfor ?>

                            </div><!-- Termina div Drag & Drop -->
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div id="result"></div>
                    </div>
                    <div class="col-sm-12 contenedor-tabs-redeem">
                      <button type="submit"><div class="col-sm-3 text-center btn-menu-top btn-general btn-derecha pull-right"><b>Publica tu lista</b></div></button>
                    </div>
                </div>
                
            </div>
        </form>
    </div>

    <!-- Footer -->
    <footer class="container-fluid footer contenedor-footer">
      <?php include('footer.php');?>
    </footer>

    <!-- Mapa de sitio -->
    <footer class="container-fluid contenedor-mapa-sito-footer">
        <?php include('footer-mapa.php');?>
    </footer>

    <!-- Modales -->
    <?php include('modales.php');?>

    <!-- Librerias -->
    <?php include('librerias.php');?>

    <!-- Select Input -->
    <script>

    function guardarLista(form){
        var f = new FormData(form);
        insertar('listas/frontendAdmin/editarLista/update/<?= $lista->id ?>',f,function(data){
            if(data.success){
                $("#result").addClass('alert alert-success').html('Se ha almacenado su lista con éxito');                
            }
        });
        return false;
    }


    function DropDown(el) {
			this.dd = el;
			this.placeholder = this.dd.children('span');
			this.opts = this.dd.find('.dropdown a');
			this.val = '';
			this.index = -1;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;
				obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					return false;
				});
				obj.opts.on('click',function(){
					var opt = $(this);
					obj.val = opt.text();
					obj.index = opt.index();
					obj.placeholder.text(obj.val);
				});
			},
			getValue : function() {
				return this.val;
			},
			getIndex : function() {
				return this.index;
			}
		}
		$(function() {
			var dd = new DropDown( $('#dd') );
		});
    $(function() {
			var dd = new DropDown( $('#dd2') );
		});
    </script>

    <!-- Add / Remove -->
    <script>
    /* Variables */
    var p = $("#participants").val();
    var row = $(".participantRow");

    /* Functions */
    function getP(){
    p = $("#participants").val();
    }

    function addRow() {
    row.clone(true, true).appendTo("#participantTable");
    }

    function removeRow(button) {
    button.closest("div").remove();
    }
    /* Doc ready */
    $(".add").on('click', function () {
    getP();
    if($("#participantTable div").length < 17) {
      addRow();
      var i = Number(p)+1;
      $("#participants").val(i);
    }
    $(this).closest("tr").appendTo("#participantTable");
    if ($("#participantTable div").length === 3) {
      $(".remove").hide();
    } else {
      $(".remove").show();
    }
    });
    $(".remove").on('click', function () {
    getP();
    if($("#participantTable div").length === 3) {
      //alert("Can't remove row.");
      $(".remove").hide();
    } else if($("#participantTable div").length - 1 ==3) {
      $(".remove").hide();
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    } else {
      removeRow($(this));
      var i = Number(p)-1;
      $("#participants").val(i);
    }
    });
    $("#participants").change(function () {
    var i = 0;
    p = $("#participants").val();
    var rowCount = $("#participantTable div").length - 2;
    if(p > rowCount) {
      for(i=rowCount; i<p; i+=1){
        addRow();
      }
      $("#participantTable #addButtonRow").appendTo("#participantTable");
    } else if(p < rowCount) {
    }
    });
    </script>
</body>
</html>
